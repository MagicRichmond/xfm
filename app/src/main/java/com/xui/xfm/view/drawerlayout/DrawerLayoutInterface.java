package com.xui.xfm.view.drawerlayout;

/**
 * Created by MaTeng on 16/11/18.
 */

public class DrawerLayoutInterface {

    public interface DrawerLayoutImpl {
        void setChildInsets(Object insets, boolean drawStatusBar);
    }

    public interface DrawerChangedListener {

        void onScroll(int offset, int width);

    }

}
