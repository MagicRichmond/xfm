package com.xui.xfm.view;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.xui.xfm.R;


/**
 * Created by fanlei on 2016/8/17.
 * ------自定义弹出框-------
 */
public class RefreshPromptDialog extends Dialog {

    public RefreshPromptDialog(Context context) {
        super(context);
    }

    public RefreshPromptDialog(Context context, int themeResId) {
        super(context, themeResId);
    }

    protected RefreshPromptDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    public static class Builder {
        private Context context;
        private String title;  //标题
        private String message;//提示消息
        private String negative_text;//消极的
        private String positive_text;//积极的
        private OnClickListener negativeListener;//消极的监听
        private OnClickListener positiveListener;//积极的监听


        public Builder(Context context) {
            this.context = context;
        }

        public Builder setTitle(String title) {
            if (title == null) {
                this.title = "\\t 确定刷新频道列表，并\\n清除之前设置的常用频道?";
            }
            this.title = title;
            return this;
        }

        public Builder setMessage(String message) {
            if (message == null) {
                this.message = "\\t 确定刷新频道列表，并\\n清除之前设置的常用频道?";
            }
            this.message = message;
            return this;
        }

        public Builder setNegativeButton(OnClickListener negativeListener) {
            if (negative_text == null) {
                this.negative_text = "否";
            }
            this.negative_text = negative_text;
            this.negativeListener = negativeListener;

            return this;
        }

        public Builder setPositionButton(OnClickListener positiveListener) {
            if (positive_text == null) {
                this.positive_text = "是";
            }
            this.positive_text = positive_text;
            this.positiveListener = positiveListener;

            return this;
        }

        private TextView tv_title_custom_dialog;  //标题
        private TextView tv_message_custom_dialog;//提示信息
        private Button btn_negative_custom_dialog;//消极
        private Button btn_positive_custom_dialog;//积极


        public RefreshPromptDialog create() {
            final RefreshPromptDialog dialog = new RefreshPromptDialog(context);
            View view = LayoutInflater.from(context).inflate(R.layout.fm_btn_refresh_dialog, null);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(view, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

            WindowManager.LayoutParams layoutParams = dialog.getWindow().getAttributes();
            layoutParams.dimAmount = 0.85f;

            Window window = dialog.getWindow();
            window.setBackgroundDrawableResource(R.drawable.fm_refresh_dialog_bg);

            window.setAttributes(layoutParams);
            window.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
//            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);//加上这一句，取消原来的标题栏，没加这句之前，发现在三星的手机上会有一条蓝色的线
//            dialog.addContentView(view, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
//            tv_title_custom_dialog = (TextView) view.findViewById(R.id.tv_message_custom_dialog);
//            tv_message_custom_dialog = (TextView) view.findViewById(R.id.tv_message_custom_dialog);
            btn_negative_custom_dialog = (Button) view.findViewById(R.id.btn_negative_custom_dialog);
            btn_positive_custom_dialog = (Button) view.findViewById(R.id.btn_positive_custom_dialog);
//            tv_title_custom_dialog.setText(title);
//            tv_message_custom_dialog.setText(message);
            btn_negative_custom_dialog.setText(negative_text);
            btn_positive_custom_dialog.setText(positive_text);
//            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            btn_negative_custom_dialog.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (negativeListener != null)
                        negativeListener.onClick(dialog, Dialog.BUTTON_NEGATIVE);
                }
            });
            btn_positive_custom_dialog.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (positiveListener != null)
                    positiveListener.onClick(dialog, Dialog.BUTTON_POSITIVE);
                }
            });
            return dialog;
        }
    }
}
