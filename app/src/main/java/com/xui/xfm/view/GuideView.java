package com.xui.xfm.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.xui.xfm.R;
import com.xui.xfm.XFMApp;

/**
 * Created by gcf on 2016/11/29.
 */

public class GuideView extends RelativeLayout {

    private Context mContext;

    public GuideView(Context context) {
        super(context);
        initView();
    }

    public GuideView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public GuideView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    private void initView() {
        mContext = XFMApp.getContext();

        View view = inflate(mContext, R.layout.set_channel_guide, null);
        LayoutParams layoutParams = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        layoutParams.addRule(CENTER_IN_PARENT);
        view.setLayoutParams(layoutParams);
        addView(view);
    }
}
