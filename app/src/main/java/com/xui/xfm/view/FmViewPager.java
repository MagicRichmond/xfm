package com.xui.xfm.view;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;

/**
 * 自定义ViewPager，当播放非常用频道时进行拦截，左右滑动设置为第4和第1个界面
 * Created by gcf on 2016/8/24.
 */
public class FmViewPager extends ViewPager {

    private boolean isScroll = true;    //  当播放非常用频道时进行拦截

    private float begin_x = 0;
    private float begin_y = 0;

    private GestureDetector mGestureDetector;

    public FmViewPager(Context context) {
        super(context);
    }

    public FmViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public interface ScrollDirectionCallback {

        void onLeftScroll();
        void onRightScroll();

    }
    private ScrollDirectionCallback mCallback;

    public void setScrollDirectionListener (ScrollDirectionCallback callback) {
        mCallback = callback;
    }


//        @Override
//    public boolean onTouchEvent(MotionEvent ev) {
//
//        switch (ev.getAction()) {
//            case MotionEvent.ACTION_DOWN:
//                begin_x = ev.getX();
//                begin_y = ev.getY();
////                Log.d(TAG, "ev.getX() - DOWN  : " + ev.getX());
//                break;
//            case MotionEvent.ACTION_UP:
////                Log.d(TAG, "ev.getX() - UP  : " + ev.getX());
//
//                if ((ev.getX() - begin_x) > 30) {
//                    if (null != mCallback) {
//                        mCallback.onRightScroll();
//                    }
//
//                    System.out.println("RRRRRRRRRRRR");
//                } else  if ((begin_x - ev.getX()) > 30){
//                    if (null != mCallback) {
//                        mCallback.onLeftScroll();
//                    }
//                    System.out.println("LLLLLLLLLLLLLLL");
//                }
//
//                break;
//        }
//
//        return super.onTouchEvent(ev);
//    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        if (isScroll) {
            return super.onInterceptTouchEvent(ev);
        } else {
            return false;
        }

//        return super.onInterceptTouchEvent(ev);
    }

    public void setIsScroll(boolean isScroll) {
        this.isScroll = isScroll;
    }
}
















