package com.xui.xfm.view;

import android.content.Context;
import android.logic.XuiManager;
import android.media.AudioManager;
import android.os.Handler;
import android.os.Message;
import android.os.RemoteException;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Display;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.xui.xfm.R;
import com.xui.xfm.XFMApp;
import com.xui.xfm.utils.SharedPrefUtils;

/**
 * Created by gcf on 2016/8/15.
 */
public class VolumeLayout extends RelativeLayout {

    private static final String TAG = "VolumeLayout";

    private XuiManager mXuiManager;

    private GestureDetector mGestureDetector;
    //音量图片
    public View mVolumeBrightnessLayout;
    private ImageView mOperationBg;
    //判断声音图片隐藏前是否再次操作音量
    private boolean soundHide = false;
    //判断是否是滑动到静音，是的话禁止单击恢复音量
    private boolean isSilene;
    //判断当前是否上下滑动状态
    private boolean isScroll;

    //  view Height
    public static int sViewHeight;

    private Context mContext = XFMApp.getContext();

    /**
     * 最大声音
     */
    private int mMaxVolume;

    /**
     * 当前声音
     */
    private int mVolume = -1;

    private AudioManager mAudioManager;

    private OnClickListener onClickListener;


    public VolumeLayout(Context context) {
        super(context);
        initView(context);
    }

    public VolumeLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public VolumeLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initView(context);
    }

    private void initView(Context context) {
        mXuiManager = XFMApp.getXuiManager();

        final View fm_pager = inflate(context, R.layout.fm_pager_item, null);
        LayoutParams fm_params = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        fm_params.addRule(RelativeLayout.CENTER_IN_PARENT);
        fm_pager.setLayoutParams(fm_params);
        addView(fm_pager);

        View view = inflate(context, R.layout.sound_relative, null);
        LayoutParams params = new LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.CENTER_IN_PARENT);
        view.setLayoutParams(params);

        mVolumeBrightnessLayout = view.findViewById(R.id.operation_volume_brightness);
        mOperationBg = (ImageView) view.findViewById(R.id.operation_bg);
        addView(view);

        mAudioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        mMaxVolume = mAudioManager
                .getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        mGestureDetector = new GestureDetector(context, new MyGestureListener());
    }

    public void setOnClickListener(OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
        setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    public class MyGestureListener extends GestureDetector.SimpleOnGestureListener {

        boolean isUpDown = false;
        boolean isLeftRight = false;

        @Override
        public boolean onDown(MotionEvent e) {
            isUpDown = false;
            isLeftRight = false;

            return super.onDown(e);
        }

        /**
         * 单击静音
         *
         * @param e
         * @return
         */
        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            onVolumeSilence();  //  单击静音

            return super.onSingleTapConfirmed(e);
        }

        @Override
        public boolean onSingleTapUp(MotionEvent e) {
            if (onClickListener != null) {
                onClickListener.onClick(VolumeLayout.this);
            }
            return super.onSingleTapUp(e);
        }

        /**
         * 滑动
         */
        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2,
                                float velocityX, float velocityY) {
            boolean isCommonItem = SharedPrefUtils.getIsCommonChannel("isCommonChannel", true, mContext);
            isScroll = true;

            int windowHeight = sViewHeight;

            float minMove = 20;        //最小滑动距离
            float minVelocity = 0;     //最小滑动速度

            float beginY = e1.getY();
            float endY = e2.getY();
            float beginX = e1.getX();
            float endX = e2.getX();


            if (beginY - endY > minMove && Math.abs(velocityY) > minVelocity
                    && Math.abs(beginX - endX) * 2 < Math.abs(beginY - endY) && !isLeftRight) {
                isUpDown = true;
//                System.out.println("isUPDown = " + isUpDown);
//                Log.d(TAG, "上滑");
                float mOldY = e1.getY();
                int y = (int) e2.getRawY();
                onVolumeSlide((mOldY - y) / windowHeight);
                isLeftRight = false;
            } else if (endY - beginY > minMove && Math.abs(velocityY) > minVelocity
                    && Math.abs(beginX - endX) * 2 < Math.abs(beginY - endY) && !isLeftRight) {
                isUpDown = true;
//                System.out.println("isUPDown = " + isUpDown);

//                Log.d(TAG, "下滑");
                float mOldY = e1.getY();
                int y = (int) e2.getRawY();
                onVolumeSlide((mOldY - y) / windowHeight);
                isLeftRight = false;
            } else if (endX - beginX > 50 && Math.abs(velocityY) > minVelocity
                    && Math.abs(beginX - endX) > 3 * Math.abs(beginY - endY) && !isUpDown) {
                isLeftRight = true;
//                Log.d(TAG, "左滑" + " isUpDown =" + isUpDown);   //  当前播放非常用频道，左滑跳至第1页
                if (!isCommonItem) {
                    XFMApp.getHandler().sendEmptyMessage(102);
                }
                isUpDown = false;
            } else if (beginX - endX > 50 && Math.abs(velocityY) > minVelocity
                    && Math.abs(beginX - endX) > 3 * Math.abs(beginY - endY) && !isUpDown) {
                isLeftRight = true;
//                Log.d(TAG, "右滑" + " isUpDown =" + isUpDown);   //  当前播放非常用频道，右滑跳至第4页
                if (!isCommonItem) {
                    XFMApp.getHandler().sendEmptyMessage(103);
                }
                isUpDown = false;
            }

            return super.onScroll(e1, e2, velocityX, velocityY);
        }

    }

    /**
     * 静音
     */
    public void onVolumeSilence() {
        boolean isOff = false;

        try {
            mXuiManager.setMuteOnOff();
            isOff = mXuiManager.getMuteOnOff();
            System.out.println("=== " + isOff);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        if (!isOff) {
            if (!isSilene) {
                Log.d(TAG, "静音");
                soundHide = true;
                mVolumeBrightnessLayout.setVisibility(VISIBLE);
                mOperationBg.setImageResource(R.drawable.tips_volume01);

//                mAudioManager.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
            }
        } else {
            Log.d(TAG, "开音");
            soundHide = false;
            mVolumeBrightnessLayout.setVisibility(GONE);

        }

    }

    /**
     * 滑动改变声音大小
     *
     * @param percent
     */
    private void onVolumeSlide(float percent) {
        try {
            Log.d(TAG, "Vol = " + mXuiManager.getSysMainVol());
            if (mXuiManager.getMuteOnOff()) {
                System.out.println("1 = " + mXuiManager.getMuteOnOff());
                mXuiManager.setMuteOnOff();
                mXuiManager.getMuteOnOff();
                System.out.println("2 = " + mXuiManager.getMuteOnOff());

            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }

        if (mVolume == -1) {
            mVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
            if (mVolume < 0)
                mVolume = 0;

            // 显示
            soundHide = false;
            mVolumeBrightnessLayout.setVisibility(View.VISIBLE);
        }

        int index = (int) (percent * mMaxVolume) + mVolume;
        if (index > mMaxVolume)
            index = mMaxVolume;
        else if (index < 0)
            index = 0;
        soundHide = true;
        // 变更声音
        mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, index, 0);

        isSilene = false;
        try {
            switch (index) {
                case 0:
                    soundHide = false;
                    mOperationBg.setImageResource(R.drawable.tips_volume01);
                    isSilene = true;
//                    mXuiManager.setVolValue(0);
                    Log.d(TAG, "0");
//                    mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, index, 0);
                    break;
                case 1:
                    mOperationBg.setImageResource(R.drawable.tips_volume02);
//                    mXuiManager.setVolValue(2);
                    Log.d(TAG, "2");
//                    mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, index, 0);
                    break;
                case 2:
                    mOperationBg.setImageResource(R.drawable.tips_volume03);
//                    mXuiManager.setVolValue(4);
                    Log.d(TAG, "4");
//                    mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, index, 0);
                    break;
                case 3:
                    mOperationBg.setImageResource(R.drawable.tips_volume04);
//                    mXuiManager.setVolValue(6);
                    Log.d(TAG, "6");
//                    mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, index, 0);
                    break;
                case 4:
                    mOperationBg.setImageResource(R.drawable.tips_volume05);
//                    mXuiManager.setVolValue(8);
                    Log.d(TAG, "8");
//                    mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, index, 0);
                    break;
                case 5:
                    mOperationBg.setImageResource(R.drawable.tips_volume06);
//                    mXuiManager.setVolValue(10);
                    Log.d(TAG, "10");
//                    mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, index, 0);
                    break;
                case 6:
                    mOperationBg.setImageResource(R.drawable.tips_volume07);
//                    mXuiManager.setVolValue(12);
                    Log.d(TAG, "12");
//                    mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, index, 0);
                    break;
                case 7:
                    mOperationBg.setImageResource(R.drawable.tips_volume08);
//                    mXuiManager.setVolValue(14);
                    Log.d(TAG, "14");
//                    mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, index, 0);
                    break;
                case 8:
                    mOperationBg.setImageResource(R.drawable.tips_volume09);
//                    mXuiManager.setVolValue(16);
                    Log.d(TAG, "16");
//                    mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, index, 0);
                    break;
                case 9:
                    mOperationBg.setImageResource(R.drawable.tips_volume10);
//                    mXuiManager.setVolValue(18);
                    Log.d(TAG, "18");
//                    mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, index, 0);
                    break;
                case 10:
                    mOperationBg.setImageResource(R.drawable.tips_volume11);
//                    mXuiManager.setVolValue(20);
                    Log.d(TAG, "20");
//                    mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, index, 0);
                    break;
                case 11:
                    mOperationBg.setImageResource(R.drawable.tips_volume12);
//                    mXuiManager.setVolValue(22);
                    Log.d(TAG, "22");
//                    mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, index, 0);
                    break;
                case 12:
                    mOperationBg.setImageResource(R.drawable.tips_volume13);
//                    mXuiManager.setVolValue(24);
                    Log.d(TAG, "24");
//                    mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, index, 0);
                    break;
                case 13:
                    mOperationBg.setImageResource(R.drawable.tips_volume14);
//                    mXuiManager.setVolValue(26);
                    Log.d(TAG, "26");
//                    mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, index, 0);
                    break;
                case 14:
                    mOperationBg.setImageResource(R.drawable.tips_volume15);
//                    mXuiManager.setVolValue(28);
                    Log.d(TAG, "28");
//                    mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, index, 0);
                    break;
                case 15:
                    mOperationBg.setImageResource(R.drawable.tips_volume16);
//                    mXuiManager.setVolValue(30);
                    Log.d(TAG, "30");
//                    mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, index, 0);
                    break;
                case 16:
                    mOperationBg.setImageResource(R.drawable.tips_volume16);
//                    mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, index, 0);
//                    mXuiManager.setVolValue(30);
                    Log.d(TAG, "30");
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * 定时隐藏
     */
    private Handler mDismissHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (soundHide) {
                mVolumeBrightnessLayout.setVisibility(View.GONE);
                isScroll = false;
            } else {
                isScroll = true;
            }
        }
    };

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (mGestureDetector.onTouchEvent(event))
            return true;

        this.mGestureDetector.onTouchEvent(event);
        // 处理手势结束
        switch (event.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_UP:
                if (isScroll) {
                    endGesture();
                }
                break;
            default:
                break;
        }
        return super.onTouchEvent(event);
    }

    /**
     * 手势结束
     */
    private void endGesture() {
//        soundHide = true;
        mVolume = -1;
        // 隐藏
        mDismissHandler.removeMessages(0);
        mDismissHandler.sendEmptyMessageDelayed(0, 300);
    }

    public interface OnClickListener {
        void onClick(View view);
    }

}
