package com.xui.xfm.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.View;

import com.xui.xfm.R;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by gcf on 2016/11/22.
 */

public class FreqView extends View {

    private float width, height;
    private float rectWidth;
    private float rect_1, rect_2, rect_3, rect_4;

    private Paint mPaint;
    private RectF r1, r2, r3, r4;

    private int random;

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == 98) {
                FreqView.this.invalidate();
            }
        }
    };


    public FreqView(Context context) {
        super(context);
    }

    public FreqView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public FreqView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        height = View.MeasureSpec.getSize(heightMeasureSpec);
        width = View.MeasureSpec.getSize(widthMeasureSpec);
        rectWidth = width / 5.5f;

        random = (int) (height / 6);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                rect_1 = new Random().nextInt(random);
                rect_2 = new Random().nextInt(random);
                rect_3 = new Random().nextInt(random);
                rect_4 = new Random().nextInt(random);

                mHandler.sendEmptyMessage(98);
            }
        }, 300);


        r1 = new RectF((float) 0, rect_1 * 5, (float) (rectWidth), (float) (height * 0.8));
        r2 = new RectF((float) (rectWidth * 1.5), rect_2 * 5, (float) (rectWidth * 2.5), (float) (height * 0.8));
        r3 = new RectF((float) (rectWidth * 3), rect_3 * 5, (float) (rectWidth * 4), (float) (height * 0.8));
        r4 = new RectF((float) (rectWidth * 4.5), rect_4 * 5, (float) (rectWidth * 5.5), (float) (height * 0.8));

        mPaint = new Paint();
        mPaint.setColor(getResources().getColor(R.color.fm_list_freq_pic));
        mPaint.setStyle(Paint.Style.FILL);
        mPaint.setAntiAlias(true);
        canvas.drawRect(r1, mPaint);
        canvas.drawRect(r2, mPaint);
        canvas.drawRect(r3, mPaint);
        canvas.drawRect(r4, mPaint);
    }
}
















