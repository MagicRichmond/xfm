package com.xui.xfm.view;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;

import com.xui.xfm.R;
import com.xui.xfm.XFMApp;
import com.xui.xfm.utils.SharedPrefUtils;

/**
 * Created by gcf on 2016/8/17.
 * ------自定义弹出框-------
 */
public class SettingsPromptDialog extends Dialog {

    public SettingsPromptDialog(Context context) {
        super(context);
    }

    public SettingsPromptDialog(Context context, int themeResId) {
        super(context, themeResId);
    }

    protected SettingsPromptDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    public static class Builder {
        private Context context;
        private String title;  //标题
        private String message;//提示消息
        private String negative_text;//消极的
        private String positive_text;//积极的
        private String neutral_text;//忽略的

        private OnClickListener negativeListener;//消极的监听
        private OnClickListener positiveListener;//积极的监听
        private OnClickListener neutralListener;//忽略的监听

        public Builder(Context context) {
            this.context = context;
        }

        public Builder setTitle(String title) {
            if (title == null) {
                this.title = "提醒";
            }
            this.title = title;
            return this;
        }

        public Builder setMessage(String message) {
            if (message == null) {
                this.message = "您没有填写提示信息哦";
            }
            this.message = message;
            return this;
        }

        public Builder setNegativeButton(OnClickListener negativeListener) {
            if (negative_text == null) {
//                this.negative_text = "取消";
            }
            this.negative_text = negative_text;
            this.negativeListener = negativeListener;

            return this;
        }

        public Builder setPositionButton(OnClickListener positiveListener) {
            if (positive_text == null) {
//                this.positive_text = "确定";
            }
            this.positive_text = positive_text;
            this.positiveListener = positiveListener;

            return this;
        }

        public Builder setNeutralButton(OnClickListener neutralListener) {
            if (positive_text == null) {
//                this.neutral_text = "忽略";
            }
            this.neutral_text = neutral_text;
            this.neutralListener = neutralListener;

            return this;
        }

//        private TextView tv_title_custom_dialog;  //标题
//        private TextView tv_message_custom_dialog;    //提示信息
        private Button btn_negative_custom_dialog;//消极
        private Button btn_positive_custom_dialog;//积极
        private Button btn_neutral_custom_dialog;//积极

        private View settings_bg;


        public SettingsPromptDialog create() {
            final SettingsPromptDialog dialog = new SettingsPromptDialog(context);
            View view = LayoutInflater.from(context).inflate(R.layout.fm_btn_settings_dialog, null);
            //加上这一句，取消原来的标题栏，没加这句之前，发现在三星的手机上会有一条蓝色的线

            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//            dialog.getWindow().setAttributes(layoutParams);
//            dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            dialog.setContentView(view, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

            WindowManager.LayoutParams layoutParams = dialog.getWindow().getAttributes();
            layoutParams.dimAmount = 0.95f;

            Window window = dialog.getWindow();
            window.setBackgroundDrawableResource(R.drawable.fm_settings_dialog_bg);
            window.setAttributes(layoutParams);
            window.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);

//            tv_title_custom_dialog = (TextView) view.findViewById(R.id.tv_title_custom_dialog);
//            tv_message_custom_dialog = (TextView) view.findViewById(R.id.tv_message_custom_dialog);

            boolean isFmMode = SharedPrefUtils.getFmMode("isFmMode", true, XFMApp.getContext());

            if (isFmMode) {
                btn_negative_custom_dialog = (Button) view.findViewById(R.id.btn_negative_custom_dialog_am);
                btn_negative_custom_dialog.setVisibility(View.VISIBLE);
            } else {
                btn_negative_custom_dialog = (Button) view.findViewById(R.id.btn_negative_custom_dialog_fm);
                btn_negative_custom_dialog.setVisibility(View.VISIBLE);
            }
            btn_positive_custom_dialog = (Button) view.findViewById(R.id.btn_positive_custom_dialog);
            btn_neutral_custom_dialog = (Button) view.findViewById(R.id.btn_neutral_custom_dialog);
//            tv_title_custom_dialog.setText(title);
//            tv_message_custom_dialog.setText(message);
            btn_negative_custom_dialog.setText(negative_text);
            btn_positive_custom_dialog.setText(positive_text);
            btn_neutral_custom_dialog.setText(neutral_text);

//            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.LTGRAY));
            btn_negative_custom_dialog.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (negativeListener != null)
                        negativeListener.onClick(dialog, Dialog.BUTTON_NEGATIVE);
                }
            });
            btn_positive_custom_dialog.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (positiveListener != null)
                        positiveListener.onClick(dialog, Dialog.BUTTON_POSITIVE);
                }
            });
            btn_neutral_custom_dialog.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (neutralListener != null)
                        neutralListener.onClick(dialog, Dialog.BUTTON_POSITIVE);
                }
            });
            return dialog;
        }
    }
}
