package com.xui.xfm.view;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * 设置常用频道VP
 * Created by gcf on 2016/8/24.
 */
public class SetRadioViewPager extends ViewPager {

    public SetRadioViewPager(Context context) {
        super(context);
    }

    public SetRadioViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /**
     * 当未选中常用频道时，禁止上面点击滑动
     * @param ev
     * @return
     */
    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        if (isClick) {
            return super.onInterceptTouchEvent(ev);
        } else {
            return false;
        }
    }

    private boolean isClick;

    public void setClick(boolean isClick) {
        this.isClick = isClick;
    }
}
















