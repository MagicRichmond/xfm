package com.xui.xfm.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Richmond on 2016/11/19.
 */

public class DbHelper extends SQLiteOpenHelper {

    private static final String DB_NAME = "channel.db";
    private static final int VERSION = 1;

    public static final String TABLE_FM_CHANNEL = "fm_channel";
    public static final String TABLE_AM_CHANNEL = "am_channel";
    public static final String TABLE_FM_COMMON_CHANNEL_NUM = "fm_common_channel_num";
    public static final String TABLE_AM_COMMON_CHANNEL_NUM = "am_common_channel_num";

    public DbHelper(Context context) {
        super(context, DB_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS " + TABLE_FM_CHANNEL
                + " (_id integer primary key, number varchar(10), freq varchar(10))");
        db.execSQL("CREATE TABLE IF NOT EXISTS " + TABLE_AM_CHANNEL
                + " (_id integer primary key, number varchar(10), freq varchar(10))");
        db.execSQL("CREATE TABLE IF NOT EXISTS " + TABLE_FM_COMMON_CHANNEL_NUM
                + " (_id integer primary key, first varchar(10), second varchar(10), third varchar(10), fourth varchar(10))");
        db.execSQL("CREATE TABLE IF NOT EXISTS " + TABLE_AM_COMMON_CHANNEL_NUM
                + " (_id integer primary key, first varchar(10), second varchar(10), third varchar(10), fourth varchar(10))");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
