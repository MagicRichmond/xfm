package com.xui.xfm;

import android.app.Application;
import android.content.Context;
import android.logic.XuiManager;

import com.xui.xfm.activity.FmActivity;

/**
 * Created by Richmond on 2016/11/20.
 */

public class XFMApp extends Application {

    private static XuiManager sXuiManager;

    private static Context instance;

    private static FmActivity.MyHandler myHandler;

    @Override
    public void onCreate() {
        instance = getApplicationContext();
    }

    public static Context getContext() {
        return instance;
    }

    public static void setHandler(FmActivity.MyHandler handler) {
        myHandler = handler;
    }

    public static FmActivity.MyHandler getHandler() {
        return myHandler;
    }

    public static void setXuiManager(XuiManager xuiManager) {
        sXuiManager = xuiManager;
    }

    public static XuiManager getXuiManager() {
        return sXuiManager;
    }

}
