package com.xui.xfm.bean;

/**
 * Created by Richmond on 2016/11/19.
 */

public class FmChannel {

    private int num;
    private double freq;

    public FmChannel(int num, double freq) {
        this.num = num;
        this.freq = freq;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public double getFreq() {
        return freq;
    }

    public void setFreq(double freq) {
        this.freq = freq;
    }
}
