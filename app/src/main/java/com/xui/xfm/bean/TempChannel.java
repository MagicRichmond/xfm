package com.xui.xfm.bean;

/**
 * Created by Richmond on 2016/11/20.
 */

public class TempChannel {

    private double freq;

    public TempChannel(double freq) {
        this.freq = freq;
    }

    public double getFreq() {
        return freq;
    }

    public void setFreq(double freq) {
        this.freq = freq;
    }
}
