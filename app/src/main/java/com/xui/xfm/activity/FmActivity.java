package com.xui.xfm.activity;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Point;
import android.logic.XuiContext;
import android.logic.XuiManager;
import android.logic.modle.ChannelDetail;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.IFmCallback;
import android.os.ISystemCallback;
import android.os.Message;
import android.os.RemoteException;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.xui.xfm.R;
import com.xui.xfm.XFMApp;
import com.xui.xfm.adapter.MyListAdapter;
import com.xui.xfm.adapter.MyPagerAdapter;
import com.xui.xfm.bean.AmChannel;
import com.xui.xfm.bean.FmChannel;
import com.xui.xfm.db.DbHelper;
import com.xui.xfm.utils.ChannelListUtils;
import com.xui.xfm.utils.SharedPrefUtils;
import com.xui.xfm.view.FmViewPager;
import com.xui.xfm.view.RefreshPromptDialog;
import com.xui.xfm.view.SettingsPromptDialog;
import com.xui.xfm.view.VolumeLayout;
import com.xui.xfm.view.drawerlayout.DrawerLayout;
import com.xui.xfm.view.drawerlayout.DrawerLayoutInterface;

import java.util.ArrayList;
import java.util.List;

import static com.xui.xfm.R.layout.fm_pager;
import static com.xui.xfm.R.styleable.Panel;
import static com.xui.xfm.db.DbHelper.TABLE_FM_CHANNEL;

public class FmActivity extends AppCompatActivity implements View.OnClickListener, VolumeLayout.OnClickListener {

    private static final String TAG = "FmActivity";

    private boolean isFmMode;   //  是否Fm模式

    private TextView mBtnSettings;
    private SettingsPromptDialog.Builder mSettingsPromptDialog;
    private RefreshPromptDialog.Builder mRefreshPromptDialog;

    private View mIndicatorLine;
    private RadioGroup mRadioGroup;
    private RadioButton rbIndiccator_1, rbIndiccator_2, rbIndiccator_3, rbIndiccator_4;
    private List<RadioButton> mRadioButtons;

    //  Panel、打开背景、开关竖直布局，开关图片
    private DrawerLayout mDrawerLayout;
    private View mBtnClose, mBtnOpen, mBtnCloseLayout;

    private ChannelListUtils mChannelListUtils;
    // Fm和Am所有频道List
    private List<FmChannel> mFmChannels;
    private List<AmChannel> mAmChannels;
    // Fm和Am常用频道List
    private List<FmChannel> mFmCommonChannels;
    private List<AmChannel> mAmCommonChannels;

    private FmViewPager mFmViewPager;
    private MyPagerAdapter mFmPagerAdapter;
    private ListView mListView;
    private MyListAdapter mFmListAdapter;

    //  初始化Fm频道和Am频道
    private int initFmNum, initAmNum;
    private int curVpPosition;

    private Display mDisplay;
    private Point mPoint;
    private VolumeLayout volumeLayout;

    private DbHelper mDbHelper;
    private SQLiteDatabase db;


    private XuiManager mXuiManager;
    private IFmCallback.Stub mIFmCallback = new IFmCallback.Stub() {
        @Override
        public void radioListNotice(List list) throws RemoteException {

        }

        @Override
        public void currRadioChannelNotice(int i) throws RemoteException {
            double freq = ChannelDetail.parse(i).getFreq();
            int num = ChannelDetail.parse(i).getChannelNum();
            Log.d(TAG, " freq = " + freq);
            Log.d(TAG, " num = " + num);

            //  当底层数据变化时，更新
            if (isFmMode) {
                if (num != 0 && num != mFmChannels.get(SharedPrefUtils.getFmInitNum("initFmNum", 1, FmActivity.this) - 1).getNum()) {
                    SharedPrefUtils.putFmInitNum("initFmNum", num, FmActivity.this);
                    XFMApp.getHandler().sendEmptyMessage(100);
                }
            } else {
                if (num != 0 && num != mAmChannels.get(SharedPrefUtils.getFmInitNum("initAmNum", 1, FmActivity.this) - 1).getNum()) {
                    SharedPrefUtils.putAmInitNum("initAmNum", num, FmActivity.this);
                    XFMApp.getHandler().sendEmptyMessage(100);
                }
            }

        }

        @Override
        public void radioLocNotice(boolean b) throws RemoteException {

        }

        @Override
        public void radioStNotice(boolean b) throws RemoteException {

        }

        @Override
        public void radioAreaNotice(int i) throws RemoteException {

        }
    };


    public class MyHandler extends Handler {
        public MyHandler() {

        }
    }

    private Handler mHandler = new MyHandler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 100:
                    int num = SharedPrefUtils.getFmInitNum("initFmNum", 1, FmActivity.this);
                    mListView.performItemClick(mListView.getChildAt(num - 1), num - 1, mListView.getItemIdAtPosition(num - 1));
                    break;
                case 101:
                    startActivity(new Intent(FmActivity.this, TrimActivity.class));
                    break;
                case 102:
                    rbIndiccator_4.performClick();
                    break;
                case 103:
                    rbIndiccator_1.performClick();
                    break;
                default:
                    break;
            }
        }
    };

    /**
     * 开启Xui Framework服务
     */
    private void initXuiService() {

        try {
            mXuiManager = (XuiManager) getSystemService(XuiContext.XUI_GENERAL_SERVICE);
            mXuiManager.registerFmCallback(mIFmCallback);
            XFMApp.setXuiManager(mXuiManager);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    private AudioManager mAudioManager;

    private boolean requestFocus() {
        if (mAudioManager == null)
            mAudioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
        // Request audio focus for playback
        int result = mAudioManager.requestAudioFocus(afChangeListener,
                // Use the music stream.
                AudioManager.STREAM_MUSIC,
                // Request permanent focus.
                AudioManager.AUDIOFOCUS_GAIN);

        return result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED;
    }

    AudioManager.OnAudioFocusChangeListener afChangeListener = new AudioManager.OnAudioFocusChangeListener() {
        public void onAudioFocusChange(int focusChange) {
            Log.d(TAG, "抢焦点");
            if (focusChange == AudioManager.AUDIOFOCUS_GAIN) {
                Log.d(TAG, "取到焦点");

            } else if (focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT) {
                // 暂时失去Audio Focus,并会很快再次获得
                Log.d(TAG, "暂时失去Audio Focus,并会很快再次获得");

            } else if (focusChange == AudioManager.AUDIOFOCUS_LOSS) {
                // Stop playback
                Log.d(TAG, "长时间焦点");

            } else if (focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK) {
                Log.d(TAG, "短时间焦点");
            }
        }
    };

    private int width;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fm);

        DisplayMetrics metric = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metric);
        width = metric.widthPixels;
        int height = metric.heightPixels;
        float density = metric.density;
        int densityDpi = metric.densityDpi;

//        requestFocus();

        try {
            initXuiService();
            mXuiManager.setSystemState(1);  //  Fm模式
        } catch (RemoteException e) {
            e.printStackTrace();
        }

        initView();
        initData();
        initEvent();

        //  设置初始化播放的频道
        if (isFmMode) {
            mListView.performItemClick(mListView.getChildAt(initFmNum - 1), initFmNum - 1,
                    mListView.getItemIdAtPosition(initFmNum - 1));
        } else {
            mListView.performItemClick(mListView.getChildAt(initAmNum - 1), initAmNum - 1,
                    mListView.getItemIdAtPosition(initAmNum - 1));
        }
    }

    private void initView() {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.slidingMenu);
        mBtnOpen = findViewById(R.id.btn_open);
        mBtnClose = mDrawerLayout.findViewById(R.id.btn_close);
        mBtnCloseLayout = mDrawerLayout.findViewById(R.id.btn_close_layout);
        mDrawerLayout.setmEdgeSize(mBtnOpen.getWidth());
        mDrawerLayout.setScrimColor(Color.argb(217, 0, 0, 0));
        mRadioGroup = (RadioGroup) findViewById(R.id.rg_fm);
        mBtnSettings = (TextView) findViewById(R.id.btn_settings);
        mFmViewPager = (FmViewPager) findViewById(R.id.vp_fm);
        mListView = (ListView) findViewById(R.id.panelListView);

        ViewTreeObserver viewTreeObserver = mFmViewPager.getViewTreeObserver();
        viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                VolumeLayout.sViewHeight = mFmViewPager.getMeasuredHeight();
            }
        });

        mIndicatorLine = findViewById(R.id.indicator_line);
        rbIndiccator_1 = (RadioButton) findViewById(R.id.rb_indicator_01);
        rbIndiccator_2 = (RadioButton) findViewById(R.id.rb_indicator_02);
        rbIndiccator_3 = (RadioButton) findViewById(R.id.rb_indicator_03);
        rbIndiccator_4 = (RadioButton) findViewById(R.id.rb_indicator_04);
        mRadioButtons = new ArrayList<>();
        mRadioButtons.add(rbIndiccator_1);
        mRadioButtons.add(rbIndiccator_2);
        mRadioButtons.add(rbIndiccator_3);
        mRadioButtons.add(rbIndiccator_4);

    }

    private void initData() {
        isFmMode = SharedPrefUtils.getFmMode("isFmMode", true, this);

        //  设置初始频道
        try {
            ChannelDetail channelDetail = mXuiManager.getSpChRadioFreqAndFlag();
            Log.d(TAG, " init Num = " + channelDetail.getChannelNum());
            Log.d(TAG, " init Freq = " + channelDetail.getFreq());
            if (isFmMode) {
                if (channelDetail.getChannelNum() != 0) {
                    initFmNum = channelDetail.getChannelNum();
                } else {
                    initFmNum = SharedPrefUtils.getFmInitNum("initFmNum", 1, this);
                }
            } else {
                if (channelDetail.getChannelNum() != 0) {
                    initAmNum = channelDetail.getChannelNum();
                } else {
                    initAmNum = SharedPrefUtils.getAmInitNum("initAmNum", 1, this);
                }
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }

        mDisplay = getWindowManager().getDefaultDisplay();
        mPoint = new Point();
        mDisplay.getSize(mPoint);

        XFMApp.setHandler((MyHandler) mHandler);

        mChannelListUtils = new ChannelListUtils();
        if (isFmMode) {
            mFmChannels = mChannelListUtils.getFmChannels();
            mFmCommonChannels = mChannelListUtils.getFmCommonChannels();

            rbIndiccator_1.setText(mFmCommonChannels.get(0).getFreq() + "");
            rbIndiccator_2.setText(mFmCommonChannels.get(1).getFreq() + "");
            rbIndiccator_3.setText(mFmCommonChannels.get(2).getFreq() + "");
            rbIndiccator_4.setText(mFmCommonChannels.get(3).getFreq() + "");
        } else {
            mAmChannels = mChannelListUtils.getAmChannels();
            mAmCommonChannels = mChannelListUtils.getAmCommonChannels();

            rbIndiccator_1.setText(mAmCommonChannels.get(0).getFreq() + "");
            rbIndiccator_2.setText(mAmCommonChannels.get(1).getFreq() + "");
            rbIndiccator_3.setText(mAmCommonChannels.get(2).getFreq() + "");
            rbIndiccator_4.setText(mAmCommonChannels.get(3).getFreq() + "");
        }

        mFmListAdapter = new MyListAdapter(isFmMode, mFmChannels, mAmChannels);
        mListView.setAdapter(mFmListAdapter);
        mFmPagerAdapter = new MyPagerAdapter(isFmMode, mFmCommonChannels, mAmCommonChannels);
        mFmViewPager.setAdapter(mFmPagerAdapter);

        mDbHelper = new DbHelper(this);
        db = mDbHelper.getWritableDatabase();

        volumeLayout = new VolumeLayout(this);
    }

    private void initEvent() {
        mBtnSettings.setOnClickListener(this);

        rbIndiccator_1.setOnClickListener(this);
        rbIndiccator_2.setOnClickListener(this);
        rbIndiccator_3.setOnClickListener(this);
        rbIndiccator_4.setOnClickListener(this);

        mBtnCloseLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDrawerLayout.closeDrawers();
            }
        });
        mDrawerLayout.setChangedListener(new DrawerLayoutInterface.DrawerChangedListener() {
            @Override
            public void onScroll(int offset, int width) {
                if (mBtnOpen.getMeasuredWidth() <= offset && mBtnOpen.getVisibility() != View.INVISIBLE) {
                    mBtnOpen.setVisibility(View.INVISIBLE);
                    mBtnClose.setVisibility(View.VISIBLE);
                }

                if (offset == 0 && mBtnOpen.getVisibility() == View.INVISIBLE) {
                    mBtnClose.setRotation(0);
                    mBtnClose.setVisibility(View.INVISIBLE);

                    TranslateAnimation translateAnimation = new TranslateAnimation(mBtnOpen.getWidth(), 0, 0, 0);
                    translateAnimation.setDuration(200);
                    mBtnOpen.startAnimation(translateAnimation);
                    mBtnOpen.setVisibility(View.VISIBLE);

                } else if (offset == width) {
                    if (!mDrawerLayout.isDrawerOpen(Gravity.RIGHT)) {
                        ObjectAnimator rotationAnimator = ObjectAnimator.ofFloat(mBtnClose, "rotation", 0, 180);
                        rotationAnimator.setDuration(200);
                        rotationAnimator.start();
                    }
                }
            }
        });
        mBtnOpen.post(new Runnable() {
            @Override
            public void run() {
                mDrawerLayout.setmEdgeSize(mBtnOpen.getWidth());
            }
        });

        //  覆盖右下角点击，防止与侧边栏冲突
        findViewById(R.id.cover_indicator_4).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mRadioButtons.get(3).performClick();
            }
        });

        mFmViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) mIndicatorLine.getLayoutParams();
                params.width = mFmViewPager.getMeasuredWidth() / 4;
                params.leftMargin = (int) (position * params.width + positionOffset * params.width);
                mIndicatorLine.setLayoutParams(params);


            }

            @Override
            public void onPageSelected(int position) {
                mRadioButtons.get(position).setChecked(true);
                mIndicatorLine.setVisibility(View.VISIBLE);
                if (isFmMode) {
                    mFmListAdapter.setSelectItem(mFmCommonChannels.get(position).getNum() - 1);
                } else {
                    mFmListAdapter.setSelectItem(mAmCommonChannels.get(position).getNum() - 1);
                }
                curVpPosition = position;

                try {
                    if (mXuiManager.getMuteOnOff()) {
                        mXuiManager.setMuteOnOff();
                        volumeLayout.mVolumeBrightnessLayout.setVisibility(View.GONE);
                        mFmPagerAdapter.notifyDataSetChanged();
                    }
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                mPanel.setOpen(false, true);
                mDrawerLayout.closeDrawers();
                updateViewPager(position);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_settings:
                initSettingsDialog();
                break;
            case R.id.rb_indicator_01:
                checkIndicator(0);
                break;
            case R.id.rb_indicator_02:
                checkIndicator(1);
                break;
            case R.id.rb_indicator_03:
                checkIndicator(2);
                break;
            case R.id.rb_indicator_04:
                checkIndicator(3);
                break;
            default:
                break;
        }
    }

    /**
     * 选中导航RadioButton
     *
     * @param i
     */
    private void checkIndicator(int i) {
        //  更新TempChannel
        mFmPagerAdapter.resetShowFreq(isFmMode, mFmCommonChannels, mAmCommonChannels);

        //  判断上次点击是否是常用频道
        if (isFmMode) {
            if (!isCommonChannel(SharedPrefUtils.getFmInitNum("initFmNum", 1, this))) {
                mFmPagerAdapter.notifyDataSetChanged();
            }
        } else {
            if (!isCommonChannel(SharedPrefUtils.getAmInitNum("initAmNum", 1, this))) {
                mFmPagerAdapter.notifyDataSetChanged();
            }
        }


        //  当前Viewpager显示非常用频道，再点击对应RadioButton
        if (i == curVpPosition) {
            mIndicatorLine.setVisibility(View.VISIBLE);
            if (isFmMode) {
                mFmListAdapter.setSelectItem(mFmCommonChannels.get(i).getNum() - 1);
            } else {
                mFmListAdapter.setSelectItem(mAmCommonChannels.get(i).getNum() - 1);
            }
        }

        mFmViewPager.setCurrentItem(i);

        mFmViewPager.setIsScroll(true);

        SharedPrefUtils.putIsCommonChannel("isCommonChannel", true, this);
    }

    /**
     * 点击list更新ViewPager
     *
     * @param position
     */
    private void updateViewPager(int position) {
        //  更新导航条和导航点, 更新ViewPager数值
        if (!isCommonChannel(position + 1)) {
            mFmListAdapter.setSelectItem(position);

            if (isFmMode) {
                mFmPagerAdapter.setShowFreq(mFmChannels.get(position).getFreq(), curVpPosition);
            } else {
                mFmPagerAdapter.setShowFreq(mAmChannels.get(position).getFreq(), curVpPosition);
            }
            mFmPagerAdapter.notifyDataSetChanged();
            mRadioGroup.clearCheck();
            mIndicatorLine.setVisibility(View.INVISIBLE);

            mFmViewPager.setIsScroll(false);
        } else {
//            mFmViewPager.setCurrentItem(getCommonPosition(position + 1));
            mRadioButtons.get(getCommonPosition(position + 1)).performClick();
            mIndicatorLine.setVisibility(View.VISIBLE);
            mFmViewPager.setIsScroll(true);
        }
    }

    /**
     * 判断list的item是否是常用频道
     *
     * @param num
     * @return
     */
    private boolean isCommonChannel(int num) {
        if (isFmMode) {
            if (mFmCommonChannels.contains(mFmChannels.get(num - 1))) {
                SharedPrefUtils.putIsCommonChannel("isCommonChannel", true, this);
                return true;
            } else {
                SharedPrefUtils.putIsCommonChannel("isCommonChannel", false, this);
                return false;
            }
        } else {
            if (mAmCommonChannels.contains(mAmChannels.get(num - 1))) {
                return true;
            } else {
                return false;
            }
        }

    }

    /**
     * 获取list在常用频道的位置
     *
     * @param num
     * @return
     */
    private int getCommonPosition(int num) {
        if (isFmMode) {
            for (int i = 0; i < 4; i++) {
                if (mFmCommonChannels.get(i).getNum() == num) {
                    return i;
                }
            }
        } else {
            for (int i = 0; i < 4; i++) {
                if (mAmCommonChannels.get(i).getNum() == num) {
                    return i;
                }
            }
        }
        return 0;
    }

    /**
     * 设置及刷新频道
     */
    private void initSettingsDialog() {
        mSettingsPromptDialog = new SettingsPromptDialog.Builder(this);

        mSettingsPromptDialog.create().show();

        //  设置常用频道
        mSettingsPromptDialog.setPositionButton(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startActivity(new Intent(FmActivity.this, SetChannelActivity.class));
                dialog.dismiss();
            }
        });
        //  切换FM，AM
        mSettingsPromptDialog.setNegativeButton(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
//                if (isFmMode) {
//                    SharedPrefUtils.putFmMode("isFmMode", false, getApplicationContext());
//                } else {
//                    SharedPrefUtils.putFmMode("isFmMode", true, getApplicationContext());
//                }
//                startActivity(new Intent(FmActivity.this, StartActivity.class));
//                dialog.dismiss();
            }
        });
        //  刷新频道
        mSettingsPromptDialog.setNeutralButton(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                initRefreshDialog();
                dialog.dismiss();
            }
        });
    }


    private void initRefreshDialog() {
        mRefreshPromptDialog = new RefreshPromptDialog.Builder(this);

        mRefreshPromptDialog.setPositionButton(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (isFmMode) {
                    SharedPrefUtils.putFmSearch("isFmSearch", false, getApplicationContext());
                    SharedPrefUtils.putFmInitNum("initFmNum", 1, getApplicationContext());
                } else {
                    SharedPrefUtils.putAmSearch("isAmSearch", false, getApplicationContext());
                    SharedPrefUtils.putAmInitNum("initAmNum", 1, getApplicationContext());
                }
                startActivity(new Intent(FmActivity.this, StartActivity.class));
                dialog.dismiss();
                finish();
            }
        });
        mRefreshPromptDialog.setNegativeButton(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        mRefreshPromptDialog.create().show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        try {
            mXuiManager.unregisterFmCallback(new IFmCallback.Stub() {
                @Override
                public void radioListNotice(List list) throws RemoteException {

                }

                @Override
                public void currRadioChannelNotice(int i) throws RemoteException {

                }

                @Override
                public void radioLocNotice(boolean b) throws RemoteException {

                }

                @Override
                public void radioStNotice(boolean b) throws RemoteException {

                }

                @Override
                public void radioAreaNotice(int i) throws RemoteException {

                }
            });

        } catch (RemoteException e) {
            e.printStackTrace();
        }

        Log.d(TAG, "Fm Activity Destory!");
    }

    @Override
    protected void onResume() {
        super.onResume();

        try {
            mXuiManager.setSystemState(1);
        } catch (RemoteException e) {
            e.printStackTrace();
        }

        //  设置常用频道和微调后更新数据
        if (SharedPrefUtils.getIsUpData("isUpData", false, this)) {
            SharedPrefUtils.putIsUpData("isUpData", false, this);
            finish();
            startActivity(new Intent(FmActivity.this, FmActivity.class));
        }

    }
}
