package com.xui.xfm.activity;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.logic.XuiContext;
import android.logic.XuiManager;
import android.logic.modle.Channel;
import android.logic.modle.ChannelDetail;
import android.os.Bundle;
import android.os.IFmCallback;
import android.os.RemoteException;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;

import com.xui.xfm.R;
import com.xui.xfm.bean.AmChannel;
import com.xui.xfm.bean.FmChannel;
import com.xui.xfm.db.DbHelper;
import com.xui.xfm.utils.SharedPrefUtils;

import java.util.ArrayList;
import java.util.List;

import static com.xui.xfm.db.DbHelper.TABLE_FM_CHANNEL;

public class StartActivity extends AppCompatActivity {

    private static final String TAG = "StartActivity";

    private XuiManager mXuiManager;

    private boolean isFmMode;   //是否Fm模式
    private boolean isFmSearch, isAmSearch;   //列表是否搜索过

    // Fm和Am所有频道List（微调前）
    private List<FmChannel> mFmChannels = new ArrayList<>();
    private List<AmChannel> mAmChannels = new ArrayList<>();

    private List<Channel> mTempList = new ArrayList<>();

    private int initFmNum, initAmNum;   //初始化播放频道

    private DbHelper mDbHelper;
    private SQLiteDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);


        isFmMode = SharedPrefUtils.getFmMode("isFmMode", true, this);
        isFmSearch = SharedPrefUtils.getFmSearch("isFmSearch", false, this);
        isAmSearch = SharedPrefUtils.getAmSearch("isAmSearch", false, this);

        mDbHelper = new DbHelper(this);
        db = mDbHelper.getWritableDatabase();

        try {
            mXuiManager = (XuiManager) getSystemService(XuiContext.XUI_GENERAL_SERVICE);
            mXuiManager.setSystemState(1);
        } catch (RemoteException e) {
            e.printStackTrace();
        }

        if (isFmMode) {
            if (!isFmSearch) {
                setContentView(R.layout.activity_start);
                startSearchingDialog();

                //  初始化并存储到数据库
                initFmData();
            } else {
                startActivity(new Intent(StartActivity.this, FmActivity.class));
                finish();
            }
        } else {
            if (!isAmSearch) {
                setContentView(R.layout.activity_start);
                startSearchingDialog();

                initAmData();
            } else {
                startActivity(new Intent(StartActivity.this, FmActivity.class));
                finish();
            }
        }

    }

    /**
     * 搜索对话框旋转效果
     */
    private void startSearchingDialog() {
        View view = findViewById(R.id.fm_search_dialog_circle);

        Animation animation = AnimationUtils.loadAnimation(this, R.anim.circle_rotate);
        LinearInterpolator linearInterpolator = new LinearInterpolator();
        animation.setInterpolator(linearInterpolator);

        view.startAnimation(animation);
    }

    private void saveFmData() {
        Log.d(TAG, mFmChannels.size() + "");

        db.execSQL("DELETE FROM " + TABLE_FM_CHANNEL);
        for (int i = 0; i < mFmChannels.size(); i++) {
            db.execSQL("INSERT INTO " + DbHelper.TABLE_FM_CHANNEL + " (number, freq) values (?, ?)",
                    new Object[]{mFmChannels.get(i).getNum() + "", mFmChannels.get(i).getFreq() + ""});
        }

        db.execSQL("INSERT INTO " + DbHelper.TABLE_FM_COMMON_CHANNEL_NUM + "(first, second, third, fourth) values (?, ?, ?, ?)",
                new Object[]{"1", "2", "3", "4"});
    }

    private void saveAmData() {
        db.execSQL("DELETE FROM " + TABLE_FM_CHANNEL);
        for (int i = 0; i < mAmChannels.size(); i++) {
            db.execSQL("INSERT INTO " + DbHelper.TABLE_AM_CHANNEL + " (number, freq) values (?, ?)",
                    new Object[]{mAmChannels.get(i).getNum() + "", mAmChannels.get(i).getFreq() + ""});
        }

        db.execSQL("INSERT INTO " + DbHelper.TABLE_AM_COMMON_CHANNEL_NUM + "(first, second, third, fourth) values (?, ?, ?, ?)",
                new Object[]{"1", "2", "3", "4"});
    }

    private void initFmData() {

        try {
            mXuiManager.registerFmCallback(new IFmCallback.Stub() {
                @Override
                public void radioListNotice(List list) throws RemoteException {
                    Log.d(TAG, "list = " + list.size());

                    if (mTempList.size() != 0) {
                        mTempList.clear();
                    }
                    mTempList.addAll(Channel.parse(list));

                    //  bean转换
                    if (mFmChannels.size() != 0) {
                        Log.d(TAG, "mFmChannels.size() 1= " + mFmChannels.size());
                        mFmChannels.clear();
                    }
                    for (int i = 0; i < list.size(); i++) {
                        mFmChannels.add(i, new FmChannel(mTempList.get(i).getChannelNum(), mTempList.get(i).getFreq()));
                    }

                    //  处理频道少于四个情况
                    switch (mFmChannels.size()) {
                        case 0:
                            mXuiManager.radioAutoScan();
                            break;
                        case 1:
                            mFmChannels.add(mFmChannels.get(0));
                            mFmChannels.add(mFmChannels.get(0));
                            mFmChannels.add(mFmChannels.get(0));
                            break;
                        case 2:
                            mFmChannels.add(mFmChannels.get(0));
                            mFmChannels.add(mFmChannels.get(1));
                            break;
                        case 3:
                            mFmChannels.add(mFmChannels.get(0));
                            break;
                        default:
                            break;
                    }

                    if (mFmChannels.size() != 0 && initFmNum != 1) {
                        Log.d(TAG, "mFmChannels.size() = " + mFmChannels.size());

                        SharedPrefUtils.putFmSearch("isFmSearch", true, getApplicationContext());
                        saveFmData();   //  数据库存储
                        startActivity(new Intent(StartActivity.this, FmActivity.class));
                        finish();
                    }
                }

                @Override
                public void currRadioChannelNotice(int i) throws RemoteException {
//                    if (ChannelDetail.parse(i).getChannelNum() != 0) {
//                        initFmNum = ChannelDetail.parse(i).getChannelNum();
//                        Log.d(TAG, "initNum = " + initFmNum);
//                        SharedPrefUtils.putFmInitNum("initFmNum", initFmNum, getApplicationContext());
//                    }
                }

                @Override
                public void radioLocNotice(boolean b) throws RemoteException {

                }

                @Override
                public void radioStNotice(boolean b) throws RemoteException {

                }

                @Override
                public void radioAreaNotice(int i) throws RemoteException {

                }
            });

            mXuiManager.radioAutoScan();
        } catch (RemoteException e) {
            e.printStackTrace();
        }

//        mFmChannels = new ArrayList<FmChannel>();
//        mFmChannels.add(new FmChannel(1, 58.6));
//        mFmChannels.add(new FmChannel(2, 62.6));
//        mFmChannels.add(new FmChannel(3, 75.6));
//        mFmChannels.add(new FmChannel(4, 75.8));
//        mFmChannels.add(new FmChannel(5, 83.6));
//        mFmChannels.add(new FmChannel(6, 103.6));
//        mFmChannels.add(new FmChannel(7, 106.6));
//        mFmChannels.add(new FmChannel(8, 109.6));
//        mFmChannels.add(new FmChannel(9, 116.6));
//        mFmChannels.add(new FmChannel(10, 124.6));
//        mFmChannels.add(new FmChannel(11, 134.6));
//        mFmChannels.add(new FmChannel(12, 168.6));
//        mFmChannels.add(new FmChannel(13, 174.6));
//        mFmChannels.add(new FmChannel(14, 199.6));
//        mFmChannels.add(new FmChannel(15, 200.6));
//        mFmChannels.add(new FmChannel(16, 201.6));
//        mFmChannels.add(new FmChannel(17, 202.6));
//        mFmChannels.add(new FmChannel(18, 204.6));
//        mFmChannels.add(new FmChannel(19, 206.6));
//        mFmChannels.add(new FmChannel(20, 208.6));
//        mFmChannels.add(new FmChannel(21, 211.6));
//        mFmChannels.add(new FmChannel(22, 213.6));
//        mFmChannels.add(new FmChannel(23, 218.6));
//        mFmChannels.add(new FmChannel(24, 224.6));
    }

    private void initAmData() {
        mAmChannels = new ArrayList<AmChannel>();
        mAmChannels.add(new AmChannel(1, 33.6));
        mAmChannels.add(new AmChannel(2, 32.6));
        mAmChannels.add(new AmChannel(3, 24.1));
        mAmChannels.add(new AmChannel(4, 33.7));
        mAmChannels.add(new AmChannel(5, 53.1));
        mAmChannels.add(new AmChannel(6, 26.3));
        mAmChannels.add(new AmChannel(7, 13.4));
        mAmChannels.add(new AmChannel(8, 42.3));
        mAmChannels.add(new AmChannel(9, 36.3));
        mAmChannels.add(new AmChannel(10, 56.1));
        mAmChannels.add(new AmChannel(11, 44.2));
        mAmChannels.add(new AmChannel(12, 12.1));
        mAmChannels.add(new AmChannel(13, 35.5));
        mAmChannels.add(new AmChannel(14, 43.2));
        mAmChannels.add(new AmChannel(15, 42.1));
        mAmChannels.add(new AmChannel(16, 37.9));
        mAmChannels.add(new AmChannel(17, 39.8));
        mAmChannels.add(new AmChannel(18, 31.0));
        mAmChannels.add(new AmChannel(19, 18.9));
        mAmChannels.add(new AmChannel(20, 47.1));
        mAmChannels.add(new AmChannel(21, 59.4));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        try {
            mXuiManager.unregisterFmCallback(new IFmCallback.Stub() {
                @Override
                public void radioListNotice(List list) throws RemoteException {

                }

                @Override
                public void currRadioChannelNotice(int i) throws RemoteException {

                }

                @Override
                public void radioLocNotice(boolean b) throws RemoteException {

                }

                @Override
                public void radioStNotice(boolean b) throws RemoteException {

                }

                @Override
                public void radioAreaNotice(int i) throws RemoteException {

                }
            });
        } catch (RemoteException e) {
            e.printStackTrace();
        }

        Log.d(TAG, "Start Activity Destory!");
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            mXuiManager.setSystemState(1);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }
}
