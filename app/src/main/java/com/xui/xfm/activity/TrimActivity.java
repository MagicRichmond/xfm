package com.xui.xfm.activity;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.logic.XuiContext;
import android.logic.XuiManager;
import android.logic.modle.ChannelDetail;
import android.os.Bundle;
import android.os.IFmCallback;
import android.os.RemoteException;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.xui.xfm.R;
import com.xui.xfm.XFMApp;
import com.xui.xfm.bean.AmChannel;
import com.xui.xfm.bean.FmChannel;
import com.xui.xfm.db.DbHelper;
import com.xui.xfm.utils.ChannelListUtils;
import com.xui.xfm.utils.SharedPrefUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * 微调频道
 */
public class TrimActivity extends AppCompatActivity implements View.OnClickListener {

    private XuiManager mXuiManager;

    private TextView mTextView;
    // 减少，增加，保存按钮
    private Button mDecreaseBtn, mIncreaseBtn, mSaveBtn;

    private int num;
    //  当前num，上个num，下个num的频率
    private double mFreq;
    private double mPreFreq;
    private double mNextFreq;

    private int i = 3;

    private boolean isFmMode;

    // Fm和Am所有频道List
    private List<FmChannel> mFmChannels;
    private List<AmChannel> mAmChannels;
    private ChannelListUtils mChannelListUtils;

    private DbHelper mDbHelper;
    private SQLiteDatabase db;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trim);

        initXuiService();

        isFmMode = SharedPrefUtils.getFmMode("isFmMode", true, this);

        mTextView = (TextView) findViewById(R.id.trim_fm_hz);
        mDecreaseBtn = (Button) findViewById(R.id.btn_decrease_fmhz);
        mIncreaseBtn = (Button) findViewById(R.id.btn_crease_fmhz);
        mSaveBtn = (Button) findViewById(R.id.btn_save_trim);

        mDbHelper = new DbHelper(this);
        db = mDbHelper.getWritableDatabase();

        mChannelListUtils = new ChannelListUtils();
        if (isFmMode) {
            mFmChannels = new ArrayList<>();
            mFmChannels.clear();
            mFmChannels = mChannelListUtils.getFmChannels();
        } else {
            mAmChannels = new ArrayList<>();
            mAmChannels.clear();
            mAmChannels = mChannelListUtils.getAmChannels();
        }



        if (isFmMode) {
            num = SharedPrefUtils.getFmInitNum("initFmNum", 1, this);
            mFreq = mFmChannels.get(num - 1).getFreq();
        } else {
            num = SharedPrefUtils.getAmInitNum("initAmNum", 1, this);
            mFreq = mAmChannels.get(num - 1).getFreq();
        }

        mTextView.setText(mFreq + "");

        //  是否是起始或结束num
        if (num == 1) {
            mPreFreq = (new BigDecimal(Double.toString(mFreq))).subtract(new BigDecimal(Double.toString(0.3))).doubleValue();
        } else {
            if (isFmMode) {
                mPreFreq = mFmChannels.get(num - 1 - 1).getFreq();
            } else {
                mPreFreq = mAmChannels.get(num - 1 - 1).getFreq();
            }
        }
        if (isFmMode) {
            if (num == mChannelListUtils.getFmChannels().size()) {
                mNextFreq = (new BigDecimal(Double.toString(mFreq))).add(new BigDecimal(Double.toString(0.3))).doubleValue();
            } else {
                mNextFreq = mFmChannels.get(num).getFreq();
            }
        } else {
            if (num == mChannelListUtils.getAmChannels().size()) {
                mNextFreq = (new BigDecimal(Double.toString(mFreq))).add(new BigDecimal(Double.toString(0.3))).doubleValue();
            } else {
                mNextFreq = mAmChannels.get(num).getFreq();
            }
        }



        mSaveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initSave();
                finish();
            }
        });

        //  判断是否超出上下num的频率值
        if (mFreq == 87.5 || mPreFreq >=
                (new BigDecimal(Double.toString(mFreq))).subtract(new BigDecimal(Double.toString(0.1))).doubleValue()) {
            mDecreaseBtn.setEnabled(false);
            mDecreaseBtn.setBackgroundResource(R.drawable.fm_app_btn_previous_disable);
        } else {
            mDecreaseBtn.setClickable(true);
            mDecreaseBtn.setBackgroundResource(R.drawable.trim_btn_left);
        }

        if (mFreq == 108.0 || mNextFreq <=
                (new BigDecimal(Double.toString(mFreq))).add(new BigDecimal(Double.toString(0.1))).doubleValue()) {
            mIncreaseBtn.setEnabled(false);
            mIncreaseBtn.setBackgroundResource(R.drawable.fm_app_btn_next_disable);
        } else {
            mIncreaseBtn.setClickable(true);
            mIncreaseBtn.setBackgroundResource(R.drawable.trim_btn_right);
        }

        mIncreaseBtn.setOnClickListener(this);
        mDecreaseBtn.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_decrease_fmhz:

                decreaseFmHz();

                if (mPreFreq >= (new BigDecimal(Double.toString(mFreq))).subtract(new BigDecimal(Double.toString(0.1))).doubleValue()
                        || i == 1 || mFreq == 87.5) {
                    mDecreaseBtn.setClickable(false);
                    mDecreaseBtn.setBackgroundResource(R.drawable.fm_app_btn_previous_disable);
                }

                break;
            case R.id.btn_crease_fmhz:

                increaseFmHz();

                if (mNextFreq <= (new BigDecimal(Double.toString(mFreq))).add(new BigDecimal(Double.toString(0.1))).doubleValue()
                        || i == 5 || mFreq == 108.0) {
                    mIncreaseBtn.setClickable(false);
                    mIncreaseBtn.setBackgroundResource(R.drawable.fm_app_btn_next_disable);
                }

                break;
            default:
                break;
        }
    }

    private void decreaseFmHz() {
        mFreq = (new BigDecimal(Double.toString(mFreq))).subtract(new BigDecimal(Double.toString(0.1))).doubleValue();

        try {
            mXuiManager.refreshRadioFreq(num, mFreq);
        } catch (RemoteException e) {
            e.printStackTrace();
        }

        mTextView.setText(mFreq + "");
        mIncreaseBtn.setEnabled(true);
        mIncreaseBtn.setClickable(true);
        mIncreaseBtn.setBackgroundResource(R.drawable.trim_btn_right);
        i--;
    }

    private void increaseFmHz() {
        mFreq = (new BigDecimal(Double.toString(mFreq))).add(new BigDecimal(Double.toString(0.1))).doubleValue();

        try {
            mXuiManager.refreshRadioFreq(num, mFreq);
        } catch (RemoteException e) {
            e.printStackTrace();
        }

        mTextView.setText(mFreq + "");
        mDecreaseBtn.setEnabled(true);
        mDecreaseBtn.setClickable(true);
        mDecreaseBtn.setBackgroundResource(R.drawable.trim_btn_left);
        i++;
    }

    /**
     * 保存数据
     */
    private void initSave() {
        if (isFmMode) {
            db.execSQL("UPDATE " + DbHelper.TABLE_FM_CHANNEL + " set freq = ? where number = ?",
                    new Object[]{mFreq, num});
        } else {
            db.execSQL("UPDATE " + DbHelper.TABLE_AM_CHANNEL + " set freq = ? where number = ?",
                    new Object[]{mFreq, num});
        }

//        Intent intent = new Intent();
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        intent.setClass(TrimActivity.this, FmActivity.class);
//        startActivity(intent);
//        SharedPrefUtils.putIsTrim("isTrim", true, this);
        SharedPrefUtils.putIsUpData("isUpData", true, this);
        finish();
    }

    /**
     * 开启Xui Framework服务
     */
    private void initXuiService() {

        try {
            mXuiManager = (XuiManager) getSystemService(XuiContext.XUI_GENERAL_SERVICE);
            mXuiManager.registerFmCallback(new IFmCallback.Stub() {
                @Override
                public void radioListNotice(List list) throws RemoteException {

                }

                @Override
                public void currRadioChannelNotice(int i) throws RemoteException {
                    Log.d("Trim Activity", ChannelDetail.parse(i).getFreq() + "");
                }

                @Override
                public void radioLocNotice(boolean b) throws RemoteException {

                }

                @Override
                public void radioStNotice(boolean b) throws RemoteException {

                }

                @Override
                public void radioAreaNotice(int i) throws RemoteException {

                }
            });
            XFMApp.setXuiManager(mXuiManager);
        } catch (RemoteException e) {
            e.printStackTrace();
        }

    }
}


//87.5   108.0