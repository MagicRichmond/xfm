package com.xui.xfm.activity;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.xui.xfm.R;
import com.xui.xfm.adapter.SetChannelGvAdapter;
import com.xui.xfm.adapter.SetChannelVpAdapter;
import com.xui.xfm.bean.AmChannel;
import com.xui.xfm.bean.FmChannel;
import com.xui.xfm.db.DbHelper;
import com.xui.xfm.utils.ChannelListUtils;
import com.xui.xfm.utils.SharedPrefUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SetChannelActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener {

    private final static String TAG = "SetChannelActivity";

    private final static int PER_PAGE_COUNTS = 8;
    private static int PAGE_COUNT;

    // Fm，Am常用频道和非常用频道
    private ChannelListUtils mChannelListUtils;
    private List<FmChannel> mFmCommonChannels;
    private List<FmChannel> mFmUncommonChannels;
    private List<AmChannel> mAmCommonChannels;
    private List<AmChannel> mAmUncommonChannels;

    private ViewPager mViewPager;
    private SetChannelVpAdapter mSetChannelVpAdapter;
    private GridView mGridView;
    private SetChannelGvAdapter mSetChannelGvAdapter;

    private List<GridView> mGridViewList;
    private List<SetChannelGvAdapter> mSetChannelGvAdapterList;


    private RadioGroup mBottomRg;   //  底部常用频道rg
    private List<RadioButton> mRadioButtons;    //  存储底部4个常用频道rb
    private RadioButton mRadioButton_01, mRadioButton_02, mRadioButton_03, mRadioButton_04;

    //  缓存List，长度为2，用于存储交换对象
    private ArrayList<AmChannel> mAmTempList;
    private ArrayList<FmChannel> mFmTempList;
    //  第一次和第二次按下位置
    private int beginPosition, endPosition;
    //  当前ViewPager位置
    private int curVpItem;

    private Button btnSave;

    private DbHelper mDbHelper;
    private SQLiteDatabase db;

    //  第一次启动，引导界面
    private Animation animationGuide;
    //  切换频道时动画放大回弹
    private Animation animation_S2B_Gv, animation_B2M_Gv;
    private Animation animation_S2B_Rb, animation_B2M_Rb;

    private boolean isAmMode;

    private View coverView; //  覆盖VP区域，用于禁止点击和滑动

    private ImageView[] mDotViews;  //  导航点数组


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_channel);

        isAmMode = !SharedPrefUtils.getFmMode("isFmMode", true, this);

        initData();
        initView();
        initEvent();
        initGuideDot();
    }

    private void initView() {
        if (SharedPrefUtils.getIsFirst("isFirst", true, this)) {
            findViewById(R.id.guide_view).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.guide_view).setVisibility(View.GONE);
        }

        mBottomRg = (RadioGroup) findViewById(R.id.rg_bottom);
        mRadioButton_01 = (RadioButton) findViewById(R.id.rb_indicator_01);
        mRadioButton_02 = (RadioButton) findViewById(R.id.rb_indicator_02);
        mRadioButton_03 = (RadioButton) findViewById(R.id.rb_indicator_03);
        mRadioButton_04 = (RadioButton) findViewById(R.id.rb_indicator_04);

        if (isAmMode) {
            mRadioButton_01.setText(mAmCommonChannels.get(0).getFreq() + "");
            mRadioButton_02.setText(mAmCommonChannels.get(1).getFreq() + "");
            mRadioButton_03.setText(mAmCommonChannels.get(2).getFreq() + "");
            mRadioButton_04.setText(mAmCommonChannels.get(3).getFreq() + "");
        } else {
            mRadioButton_01.setText(mFmCommonChannels.get(0).getFreq() + "");
            mRadioButton_02.setText(mFmCommonChannels.get(1).getFreq() + "");
            mRadioButton_03.setText(mFmCommonChannels.get(2).getFreq() + "");
            mRadioButton_04.setText(mFmCommonChannels.get(3).getFreq() + "");
        }
        mRadioButtons = new ArrayList<>();
        mRadioButtons.add(mRadioButton_01);
        mRadioButtons.add(mRadioButton_02);
        mRadioButtons.add(mRadioButton_03);
        mRadioButtons.add(mRadioButton_04);

        btnSave = (Button) findViewById(R.id.btn_save_common);

        initGridView();
        initViewPager();

        coverView = findViewById(R.id.cover_view);

        findViewById(R.id.uncommon_tip_tv).setAlpha(0.3f);
        findViewById(R.id.uncommon_tip_left_line).setAlpha(0.3f);
        findViewById(R.id.uncommon_tip_right_line).setAlpha(0.3f);

        animationGuide = new AlphaAnimation(1f, 0.0f);
        animationGuide.setDuration(1000);

        //  点击交换后动画，先放大再回弹
        animation_S2B_Gv = new ScaleAnimation(0f, 1.3f, 0f, 1.3f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        animation_S2B_Gv.setDuration(1000);
        animation_B2M_Gv = new ScaleAnimation(1.3f, 1f, 1.3f, 1f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        animation_B2M_Gv.setDuration(100);

        animation_S2B_Rb = new ScaleAnimation(0f, 1.3f, 0f, 1.3f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        animation_S2B_Rb.setDuration(1000);
        animation_B2M_Rb = new ScaleAnimation(1.3f, 1f, 1.3f, 1f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        animation_B2M_Rb.setDuration(100);
    }

    private void initGridView() {
        mGridViewList = new ArrayList<>();
        mSetChannelGvAdapterList = new ArrayList<>();

        for (int i = 0; i < PAGE_COUNT; i++) {
            LayoutInflater layoutInflater = LayoutInflater.from(this);
            View view = layoutInflater.inflate(R.layout.set_channel_gridview, null);
            mGridView = (GridView) view.findViewById(R.id.set_channel_gridview);

            //  初始化GridView数据，分页加载，每页8个
            if (isAmMode) {
                List<AmChannel> beanList = new ArrayList<>();
                int j = i * PER_PAGE_COUNTS;
                int end = j + PER_PAGE_COUNTS;
                beanList.clear();
                while ((j < mAmUncommonChannels.size()) && (j < end)) {
                    beanList.add(mAmUncommonChannels.get(j));
                    j++;
                }
                mSetChannelGvAdapter = new SetChannelGvAdapter(this, beanList, mFmUncommonChannels, !isAmMode);
                mGridView.setAdapter(mSetChannelGvAdapter);
            } else {
                List<FmChannel> beanList = new ArrayList<>();
                int j = i * PER_PAGE_COUNTS;
                int end = j + PER_PAGE_COUNTS;
                beanList.clear();
                while ((j < mFmUncommonChannels.size()) && (j < end)) {
                    beanList.add(mFmUncommonChannels.get(j));
                    j++;
                }
                mSetChannelGvAdapter = new SetChannelGvAdapter(this, mAmUncommonChannels, beanList, !isAmMode);
                mGridView.setAdapter(mSetChannelGvAdapter);
            }

            mGridView.setOnItemClickListener(this);

            mGridViewList.add(mGridView);
            mSetChannelGvAdapterList.add(mSetChannelGvAdapter);
        }
    }

    //  item点击
    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        dealUpClickEvent(i + PER_PAGE_COUNTS * curVpItem);

        mSetChannelGvAdapterList.get(curVpItem).isInitSelected(true);
        mSetChannelGvAdapterList.get(curVpItem).setClickPosition(i);
        mSetChannelGvAdapterList.get(curVpItem).notifyDataSetChanged();

        for (int j = 0; j < PAGE_COUNT; j++) {
            if (j != curVpItem) {
                mSetChannelGvAdapterList.get(j).isInitSelected(false);
                mSetChannelGvAdapterList.get(j).notifyDataSetChanged();
            }
        }
    }

    private void initViewPager() {
        mViewPager = (ViewPager) findViewById(R.id.set_channel_vp);
        mViewPager.setOffscreenPageLimit(4);
        mSetChannelVpAdapter = new SetChannelVpAdapter(mGridViewList);
        mViewPager.setAdapter(mSetChannelVpAdapter);
        mViewPager.addOnPageChangeListener(new MyPageChangeListener());
        mViewPager.setCurrentItem(curVpItem);
    }

    //  初始化导航点
    private void initGuideDot() {
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.ll_dot);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(10, 0, 10, 0);
        mDotViews = new ImageView[PAGE_COUNT];
        for (int i = 0; i < PAGE_COUNT; i++) {
            ImageView imageView = new ImageView(this);
            imageView.setLayoutParams(params);
            imageView.setImageResource(R.drawable.set_channel_dot_item);
            if (i == 0) {
                imageView.setSelected(true);
            } else {
                imageView.setSelected(false);
            }
            mDotViews[i] = imageView;
            mDotViews[i].setAlpha(0.3f);
            linearLayout.addView(imageView);
        }
    }


    //  初始化非常用频道数据
    private void initData() {
        mChannelListUtils = new ChannelListUtils();
        mChannelListUtils.getFmChannels();
        mChannelListUtils.getAmChannels();

        if (isAmMode) {
            mAmTempList = new ArrayList<>();
            mAmCommonChannels = mChannelListUtils.getAmCommonChannels();
            mAmUncommonChannels = mChannelListUtils.getAmUncommonChannels();

            PAGE_COUNT = mAmUncommonChannels.size() % PER_PAGE_COUNTS == 0 ?
                    mAmUncommonChannels.size() / PER_PAGE_COUNTS :
                    mAmUncommonChannels.size() / PER_PAGE_COUNTS + 1;
        } else {
            mFmTempList = new ArrayList<>();
            mFmCommonChannels = mChannelListUtils.getFmCommonChannels();
            mFmUncommonChannels = mChannelListUtils.getFmUncommonChannels();

            PAGE_COUNT = mFmUncommonChannels.size() % PER_PAGE_COUNTS == 0 ?
                    mFmUncommonChannels.size() / PER_PAGE_COUNTS :
                    mFmUncommonChannels.size() / PER_PAGE_COUNTS + 1;
        }

        mDbHelper = new DbHelper(this);
        db = mDbHelper.getWritableDatabase();
    }

    private void initEvent() {
        mRadioButton_01.setOnClickListener(this);
        mRadioButton_02.setOnClickListener(this);
        mRadioButton_03.setOnClickListener(this);
        mRadioButton_04.setOnClickListener(this);

        btnSave.setOnClickListener(this);
    }

    //  处理底部常用频道按下事件
    private void dealBottomClickEvent(int i) {
        if (findViewById(R.id.guide_view).getVisibility() == View.VISIBLE) {
            findViewById(R.id.guide_view).startAnimation(animationGuide);
            findViewById(R.id.guide_view).setVisibility(View.GONE);
        }
        hideAlpha();

        endPosition = i;

        if (isAmMode) {
            mAmTempList.add(mAmCommonChannels.get(i));
            if (mAmTempList.size() == 2) {
                changeCommonData();
            }
        } else {
            mFmTempList.add(mFmCommonChannels.get(i));
            if (mFmTempList.size() == 2) {
                changeCommonData();
            }
        }
    }

    //  常用频道交换数据
    private void changeCommonData() {
        judgeBeginPosition();

        if (isAmMode) {
            Collections.swap(mAmCommonChannels, beginPosition, endPosition);
            mRadioButtons.get(beginPosition).setText(mAmCommonChannels.get(beginPosition).getFreq() + "");
            mRadioButtons.get(endPosition).setText(mAmCommonChannels.get(endPosition).getFreq() + "");
            mAmTempList.clear();
        } else {
            Collections.swap(mFmCommonChannels, beginPosition, endPosition);
            mRadioButtons.get(beginPosition).setText(mFmCommonChannels.get(beginPosition).getFreq() + "");
            mRadioButtons.get(endPosition).setText(mFmCommonChannels.get(endPosition).getFreq() + "");
            mFmTempList.clear();
        }
        mBottomRg.clearCheck();

        animation_S2B_Rb.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                findViewById(R.id.cover_rg).setVisibility(View.VISIBLE);    //  View用于遮罩，动画开始禁止点击
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mRadioButtons.get(beginPosition).startAnimation(animation_B2M_Rb);
                mRadioButtons.get(endPosition).startAnimation(animation_B2M_Rb);

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        animation_B2M_Rb.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                findViewById(R.id.cover_rg).setVisibility(View.GONE);   // 动画结束可以点击
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        if (beginPosition != endPosition) {
            mRadioButtons.get(beginPosition).startAnimation(animation_S2B_Rb);
            mRadioButtons.get(endPosition).startAnimation(animation_S2B_Rb);
        }

        showAlpha();
    }

    //  处理非常用频道按下事件,进行数据交换
    private void dealUpClickEvent(int position) {
        if (isAmMode) {
            mAmTempList.add(mAmUncommonChannels.get(position));
            mAmCommonChannels.remove(endPosition);
            mAmCommonChannels.add(endPosition, mAmTempList.get(1));

            mAmUncommonChannels.remove(position);
            mAmUncommonChannels.add(position, mAmTempList.get(0));

            changeUncommonData();


            mRadioButtons.get(endPosition).setText(mAmCommonChannels.get(endPosition).getFreq() + "");

            mAmTempList.clear();
            mBottomRg.clearCheck();
        } else {
            mFmTempList.add(mFmUncommonChannels.get(position));
            mFmCommonChannels.remove(endPosition);
            mFmCommonChannels.add(endPosition, mFmTempList.get(1));

            mFmUncommonChannels.remove(position);
            mFmUncommonChannels.add(position, mFmTempList.get(0));

            changeUncommonData();

            mRadioButtons.get(endPosition).setText(mFmCommonChannels.get(endPosition).getFreq() + "");

            mFmTempList.clear();
            mBottomRg.clearCheck();
        }

        animation_S2B_Gv.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                findViewById(R.id.cover_rg).setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                findViewById(R.id.cover_rg).setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        mRadioButtons.get(endPosition).startAnimation(animation_S2B_Gv);
    }

    //  非常用频道和常用频道交换，数据重新加载
    private void changeUncommonData() {
        initGridView();
        initViewPager();
        mViewPager.setCurrentItem(curVpItem);

        showAlpha();
    }

    //  判断第一次点击时，底部常用频道按下位置
    private void judgeBeginPosition() {
        if (isAmMode) {
            int i = mAmTempList.get(0).getNum();
            for (int j = 0; j < mAmCommonChannels.size(); j++) {
                if (mAmCommonChannels.get(j).getNum() == i) {
                    beginPosition = j;
                    return;
                }
            }
        } else {
            int i = mFmTempList.get(0).getNum();
            for (int j = 0; j < mFmCommonChannels.size(); j++) {
                if (mFmCommonChannels.get(j).getNum() == i) {
                    beginPosition = j;
                    return;
                }
            }
        }


    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rb_indicator_01:
                dealBottomClickEvent(0);
                break;
            case R.id.rb_indicator_02:
                dealBottomClickEvent(1);
                break;
            case R.id.rb_indicator_03:
                dealBottomClickEvent(2);
                break;
            case R.id.rb_indicator_04:
                dealBottomClickEvent(3);
                break;
            case R.id.btn_save_common:
                initSaveRadio();
            default:
                break;
        }
    }


    /**
     * 保存四个常用频道
     */
    private void initSaveRadio() {

        if (isAmMode) {
            db.execSQL("UPDATE " + DbHelper.TABLE_AM_COMMON_CHANNEL_NUM + " set first = ?",
                    new Object[]{mAmCommonChannels.get(0).getNum()});
            db.execSQL("UPDATE " + DbHelper.TABLE_AM_COMMON_CHANNEL_NUM + " set second = ?",
                    new Object[]{mAmCommonChannels.get(1).getNum()});
            db.execSQL("UPDATE " + DbHelper.TABLE_AM_COMMON_CHANNEL_NUM + " set third = ?",
                    new Object[]{mAmCommonChannels.get(2).getNum()});
            db.execSQL("UPDATE " + DbHelper.TABLE_AM_COMMON_CHANNEL_NUM + " set fourth = ?",
                    new Object[]{mAmCommonChannels.get(3).getNum()});
        } else {
            db.execSQL("UPDATE " + DbHelper.TABLE_FM_COMMON_CHANNEL_NUM + " set first = ?",
                    new Object[]{mFmCommonChannels.get(0).getNum()});
            db.execSQL("UPDATE " + DbHelper.TABLE_FM_COMMON_CHANNEL_NUM + " set second = ?",
                    new Object[]{mFmCommonChannels.get(1).getNum()});
            db.execSQL("UPDATE " + DbHelper.TABLE_FM_COMMON_CHANNEL_NUM + " set third = ?",
                    new Object[]{mFmCommonChannels.get(2).getNum()});
            db.execSQL("UPDATE " + DbHelper.TABLE_FM_COMMON_CHANNEL_NUM + " set fourth = ?",
                    new Object[]{mFmCommonChannels.get(3).getNum()});
        }

//        Intent intent = new Intent();
//        intent.setClass(this, FmActivity.class);
//        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        startActivity(intent);
//        SharedPrefUtils.putIsSetChannel("isSetChannel", true, this);
        SharedPrefUtils.putIsUpData("isUpData", true, this);
        finish();
    }

    //  显示和隐藏Alpha, 禁止和解禁滑动
    private void showAlpha() {
        coverView.setVisibility(View.VISIBLE);
        for (int j = 0; j < PAGE_COUNT; j++) {
            mSetChannelGvAdapterList.get(curVpItem).isInitSelected(false);
            mSetChannelGvAdapterList.get(j).isShowAlpha(true);
            mSetChannelGvAdapterList.get(j).notifyDataSetChanged();

            mDotViews[j].setAlpha(0.3f);
        }
        findViewById(R.id.uncommon_tip_tv).setAlpha(0.3f);
        findViewById(R.id.uncommon_tip_left_line).setAlpha(0.3f);
        findViewById(R.id.uncommon_tip_right_line).setAlpha(0.3f);
    }

    private void hideAlpha() {
        coverView.setVisibility(View.GONE);
        for (int j = 0; j < PAGE_COUNT; j++) {
            mSetChannelGvAdapterList.get(curVpItem).isInitSelected(false);
            mSetChannelGvAdapterList.get(j).isShowAlpha(false);
            mSetChannelGvAdapterList.get(j).notifyDataSetChanged();

            mDotViews[j].setAlpha(1.0f);
        }
        findViewById(R.id.uncommon_tip_tv).setAlpha(1.0f);
        findViewById(R.id.uncommon_tip_left_line).setAlpha(1.0f);
        findViewById(R.id.uncommon_tip_right_line).setAlpha(1.0f);
    }


    /**
     * ViewPager监听事件
     */
    class MyPageChangeListener implements ViewPager.OnPageChangeListener {

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            curVpItem = position;

            for (int i = 0; i < mDotViews.length; i++) {
                if (i == position) {
                    mDotViews[i].setSelected(true);
                } else {
                    mDotViews[i].setSelected(false);
                }
            }
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        SharedPrefUtils.putIsFirst("isFirst", false, this);
    }
}
