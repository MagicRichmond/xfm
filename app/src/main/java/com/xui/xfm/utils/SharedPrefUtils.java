package com.xui.xfm.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPrefUtils {

    /**
     * 是否Fm模式
     * @param key
     * @param value
     * @param context
     */
    public static void putFmMode(String key, boolean value, Context context) {
        SharedPreferences sp = context.getSharedPreferences("fmMode", Context.MODE_PRIVATE);
        sp.edit().putBoolean(key, value).commit();
    }

    public static boolean getFmMode(String key, boolean defValue, Context context) {
        SharedPreferences sp = context.getSharedPreferences("fmMode", Context.MODE_PRIVATE);
        return sp.getBoolean(key, defValue);
    }

    /**
     *判断是否搜索过频道
     * @param key
     * @param value
     * @param context
     */
    public static void putFmSearch(String key, boolean value, Context context) {
        SharedPreferences sp = context.getSharedPreferences("isFmSearch", Context.MODE_PRIVATE);
        sp.edit().putBoolean(key, value).commit();
    }

    public static boolean getFmSearch(String key, boolean defValue, Context context) {
        SharedPreferences sp = context.getSharedPreferences("isFmSearch", Context.MODE_PRIVATE);
        return sp.getBoolean(key, defValue);
    }

    public static void putAmSearch(String key, boolean value, Context context) {
        SharedPreferences sp = context.getSharedPreferences("isAmSearch", Context.MODE_PRIVATE);
        sp.edit().putBoolean(key, value).commit();
    }

    public static boolean getAmSearch(String key, boolean defValue, Context context) {
        SharedPreferences sp = context.getSharedPreferences("isAmSearch", Context.MODE_PRIVATE);
        return sp.getBoolean(key, defValue);
    }


    /**
     * 当前播放频道ID
     * @param key
     * @param value
     * @param context
     */
    public static void putFmInitNum(String key, int value, Context context) {
        SharedPreferences sp = context.getSharedPreferences("initFmNum", Context.MODE_PRIVATE);
        sp.edit().putInt(key, value).commit();
    }

    public static int getFmInitNum(String key, int defValue, Context context) {
        SharedPreferences sp = context.getSharedPreferences("initFmNum", Context.MODE_PRIVATE);
        return sp.getInt(key, defValue);
    }

    public static void putAmInitNum(String key, int value, Context context) {
        SharedPreferences sp = context.getSharedPreferences("initAmNum", Context.MODE_PRIVATE);
        sp.edit().putInt(key, value).commit();
    }

    public static int getAmInitNum(String key, int defValue, Context context) {
        SharedPreferences sp = context.getSharedPreferences("initAmNum", Context.MODE_PRIVATE);
        return sp.getInt(key, defValue);
    }


    public static void putIsCommonChannel(String key, boolean value, Context context) {
        SharedPreferences sp = context.getSharedPreferences("isCommonChannel", Context.MODE_PRIVATE);
        sp.edit().putBoolean(key, value).commit();
    }

    public static boolean getIsCommonChannel(String key, boolean defValue, Context context) {
        SharedPreferences sp = context.getSharedPreferences("isCommonChannel", Context.MODE_PRIVATE);
        return sp.getBoolean(key, defValue);
    }

    /**
     * 判断是否第一次启动
     * @param key
     * @param value
     * @param context
     */
    public static void putIsFirst(String key, boolean value, Context context) {
        SharedPreferences sp = context.getSharedPreferences("isFirst", Context.MODE_PRIVATE);
        sp.edit().putBoolean(key, value).commit();
    }

    public static boolean getIsFirst(String key, boolean defValue, Context context) {
        SharedPreferences sp = context.getSharedPreferences("isFirst", Context.MODE_PRIVATE);
        return sp.getBoolean(key, defValue);
    }


    /**
     * 设置常用频道时和微调时更新数据
     * @param key
     * @param value
     * @param context
     */

    public static void putIsUpData(String key, boolean value, Context context) {
        SharedPreferences sp = context.getSharedPreferences("isUpData", Context.MODE_PRIVATE);
        sp.edit().putBoolean(key, value).commit();
    }

    public static boolean getIsUpData(String key, boolean defValue, Context context) {
        SharedPreferences sp = context.getSharedPreferences("isUpData", Context.MODE_PRIVATE);
        return sp.getBoolean(key, defValue);
    }

}























