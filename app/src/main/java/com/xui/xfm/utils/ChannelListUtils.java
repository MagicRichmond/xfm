package com.xui.xfm.utils;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.xui.xfm.XFMApp;
import com.xui.xfm.bean.AmChannel;
import com.xui.xfm.bean.FmChannel;
import com.xui.xfm.db.DbHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * 获取Fm和Am列表，常用频道，及非常用频道
 * <p>
 * Created by Richmond on 2016/11/20.
 */

public class ChannelListUtils {

    private DbHelper mDbHelper;
    private SQLiteDatabase db;

    // Fm和Am所有频道List
    private List<FmChannel> mFmChannels;
    private List<AmChannel> mAmChannels;
    // Fm和Am常用频道List
    private List<FmChannel> mFmCommonChannels;
    private List<AmChannel> mAmCommonChannels;
    // Fm和Am非常用频道List
    private List<FmChannel> mFmUncommonChannels;
    private List<AmChannel> mAmUncommonChannels;

    // FM和AM常用频道number
    private int firstFmNum, secondFmNum, thirdFmNum, fourthFmNum;
    private int firstAmNum, secondAmNum, thirdAmNum, fourthAmNum;

    public ChannelListUtils() {
        mDbHelper = new DbHelper(XFMApp.getContext());
        db = mDbHelper.getWritableDatabase();
    }

    /**
     * 获取FM和AM列表List
     * @return
     */
    public List<FmChannel> getFmChannels() {
        mFmChannels = new ArrayList<>();
        Cursor cursor = db.rawQuery("select * from " + DbHelper.TABLE_FM_CHANNEL, null);
        while (cursor.moveToNext()) {
            mFmChannels.add(new FmChannel(cursor.getInt(cursor.getColumnIndex("number")),
                    cursor.getDouble(cursor.getColumnIndex("freq"))));
        }

        return mFmChannels;
    }

    public List<AmChannel> getAmChannels() {
        mAmChannels = new ArrayList<>();
        Cursor cursor = db.rawQuery("select * from " + DbHelper.TABLE_AM_CHANNEL, null);
        while (cursor.moveToNext()) {
            mAmChannels.add(new AmChannel(cursor.getInt(cursor.getColumnIndex("number")),
                    cursor.getDouble(cursor.getColumnIndex("freq"))));
        }

        return mAmChannels;
    }

    /**
     * 获取FM和AM常用频道List
     * @return
     */
    public List<FmChannel> getFmCommonChannels() {
        Cursor cursor = db.rawQuery("select * from " + DbHelper.TABLE_FM_COMMON_CHANNEL_NUM, null);
        while (cursor.moveToNext()) {
            firstFmNum = Integer.parseInt(cursor.getString(1));
            secondFmNum = Integer.parseInt(cursor.getString(2));
            thirdFmNum = Integer.parseInt(cursor.getString(3));
            fourthFmNum = Integer.parseInt(cursor.getString(4));
        }

//        System.out.println("all = " + mFmChannels.size());

        mFmCommonChannels = new ArrayList<>();
        mFmCommonChannels.add(mFmChannels.get(firstFmNum - 1));
        mFmCommonChannels.add(mFmChannels.get(secondFmNum - 1));
        mFmCommonChannels.add(mFmChannels.get(thirdFmNum - 1));
        mFmCommonChannels.add(mFmChannels.get(fourthFmNum - 1));

        return mFmCommonChannels;
    }

    public List<AmChannel> getAmCommonChannels() {
        Cursor cursor = db.rawQuery("select * from " + DbHelper.TABLE_AM_COMMON_CHANNEL_NUM, null);
        while (cursor.moveToNext()) {
            firstAmNum = Integer.parseInt(cursor.getString(1));
            secondAmNum = Integer.parseInt(cursor.getString(2));
            thirdAmNum = Integer.parseInt(cursor.getString(3));
            fourthAmNum = Integer.parseInt(cursor.getString(4));
        }

        mAmCommonChannels = new ArrayList<>();
        mAmCommonChannels.add(mAmChannels.get(firstAmNum - 1));
        mAmCommonChannels.add(mAmChannels.get(secondAmNum - 1));
        mAmCommonChannels.add(mAmChannels.get(thirdAmNum - 1));
        mAmCommonChannels.add(mAmChannels.get(fourthAmNum - 1));

        return mAmCommonChannels;
    }


    /**
     * FM和AM非常用频道List
     * @return
     */
    public List<FmChannel> getFmUncommonChannels() {
        mFmUncommonChannels = new ArrayList<>();
        for (int i = 0; i < mFmChannels.size(); i++) {
            if (i != firstFmNum - 1 && i != secondFmNum - 1 && i != thirdFmNum - 1 && i != fourthFmNum - 1) {
                mFmUncommonChannels.add(mFmChannels.get(i));
            }
        }
        return mFmUncommonChannels;
    }

    public List<AmChannel> getAmUncommonChannels() {
        mAmUncommonChannels = new ArrayList<>();
        for (int i = 0; i < mAmChannels.size(); i++) {
            if (i != firstAmNum - 1 && i != secondAmNum - 1 && i != thirdAmNum - 1 && i != fourthAmNum - 1) {
                mAmUncommonChannels.add(mAmChannels.get(i));
            }
        }

        return mAmUncommonChannels;
    }
}
