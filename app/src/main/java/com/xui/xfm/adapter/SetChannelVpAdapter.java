package com.xui.xfm.adapter;

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import java.util.List;

/**
 * Created by Richmond on 2016/12/5.
 */

public class SetChannelVpAdapter extends PagerAdapter {

    private List<GridView> mGridViewList;

    public SetChannelVpAdapter(List<GridView> gridViewList) {
        mGridViewList = gridViewList;
    }

    @Override
    public int getCount() {
        return mGridViewList.size();
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        container.addView(mGridViewList.get(position));
        return mGridViewList.get(position);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
}
