package com.xui.xfm.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.xui.xfm.R;
import com.xui.xfm.bean.AmChannel;
import com.xui.xfm.bean.FmChannel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Richmond on 2016/12/5.
 */

public class SetChannelGvAdapter extends BaseAdapter {

    private Context mContext;
    private List<AmChannel> mAmChannelList;
    private List<FmChannel> mFmChannelList;

    private int gridViewSelectPosition;

    private boolean isFmMode = true;

    private boolean isInitSelected = false; //  初始化默认无选中选项
    private boolean isShowAlpha = true; //  是否加Alpha

//    public SetChannelGvAdapter(Context context, List<Bean> beanList, int gridViewSelectPosition, int page) {
//        mContext = context;
//        this.gridViewSelectPosition = gridViewSelectPosition;
//        mBeanList = new ArrayList<>();
//        mBeanList = beanList;
//
//        int i = page * PAGE_ITEM_COUNT;
//        int end = i + PAGE_ITEM_COUNT;
//
//        while ((i < beanList.size()) && (i < end)) {
//            mBeanList.add(beanList.get(i));
//            i++;
//        }
//    }

    public SetChannelGvAdapter(Context context, List<AmChannel> amChannels, List<FmChannel> fmChannels, boolean isFmMode) {
        mContext = context;
        this.isFmMode = isFmMode;
        if (isFmMode) {
            mFmChannelList = new ArrayList<>();
            mFmChannelList = fmChannels;
        } else {
            mAmChannelList = new ArrayList<>();
            mAmChannelList = amChannels;
        }
    }

    public SetChannelGvAdapter(Context context, List<FmChannel> beanList) {
        mContext = context;
        mFmChannelList = new ArrayList<>();
        mFmChannelList = beanList;
    }


    //  判断点击位置
    public void setClickPosition(int clickPosition) {
        gridViewSelectPosition = clickPosition;
    }

    //  判断页面初始化是否有item选中
    public void isInitSelected(boolean isInitSelected) {
        this.isInitSelected = isInitSelected;
    }

    public void isShowAlpha(boolean isShowAlpha) {
        this.isShowAlpha = isShowAlpha;
    }



    @Override
    public int getCount() {
        if (isFmMode) {
            return mFmChannelList.size();
        } else {
            return mAmChannelList.size();
        }
    }

    @Override
    public Object getItem(int i) {
        if (isFmMode) {
            return mFmChannelList.get(i);
        } else {
            return mAmChannelList.get(i);
        }
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        final Holder holder;

            if (view == null) {
            holder = new Holder();
            LayoutInflater layoutInflater = LayoutInflater.from(mContext);
            view = layoutInflater.inflate(R.layout.set_channel_gridview_item, null);

            holder.mTextView = (TextView) view.findViewById(R.id.tv_gridview_item);
            holder.mBgView = (TextView) view.findViewById(R.id.bg_gridview_item);
            holder.mTextView.setHeight(201);
            holder.mBgView.setHeight(201);
            view.setTag(holder);
        } else {
            holder = (Holder) view.getTag();
        }

        if (isFmMode) {
            holder.mTextView.setText(mFmChannelList.get(i).getFreq() + "");
        } else {
            holder.mTextView.setText(mAmChannelList.get(i).getFreq() + "");
        }

        final Animation animation_S2B = new ScaleAnimation(0f, 1.3f, 0f, 1.3f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        animation_S2B.setDuration(1000);

        final Animation animation_B2M = new ScaleAnimation(1.3f, 1f, 1.3f, 1f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        animation_B2M.setDuration(100);

        if (i == gridViewSelectPosition && isInitSelected) {

            animation_S2B.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    holder.mTextView.startAnimation(animation_B2M);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });

//            holder.mTextView.setSelected(true);
//            holder.mView.setSelected(true);
            holder.mTextView.startAnimation(animation_S2B);

        } else {
//            holder.mTextView.setSelected(false);
//            holder.mView.setSelected(false);
        }

        if (isShowAlpha) {
            holder.mBgView.setAlpha(0.3f);
            holder.mTextView.setAlpha(0.3f);
        } else {
            holder.mBgView.setAlpha(1.0f);
            holder.mTextView.setAlpha(1.0f);
        }

        return view;
    }

    class Holder {
        TextView mTextView;
        TextView mBgView;
    }



}
