package com.xui.xfm.adapter;

import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.xui.xfm.R;
import com.xui.xfm.XFMApp;
import com.xui.xfm.bean.AmChannel;
import com.xui.xfm.bean.FmChannel;
import com.xui.xfm.bean.TempChannel;
import com.xui.xfm.view.VolumeLayout;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Magic on 2016/9/4.
 */
public class MyPagerAdapter extends PagerAdapter {

    private static final String TAG = "MyPagerAdapter";

    private TextView mTvFreq;
    private double mFreq;

    private VolumeLayout view;

    private boolean isFmMode;

    private List<TempChannel> mTempChannels = new ArrayList<>();  //  用于界面显示freq
    private int curVpPosition;  //  当前ViewPager位置

    public MyPagerAdapter(boolean isFmMode, List<FmChannel> mFmCommonChannels, List<AmChannel> mAmCommonChannels) {
        this.isFmMode = isFmMode;
        mTempChannels.clear();
        if (isFmMode) {
            for (int i = 0; i < 4; i++) {
                mTempChannels.add(new TempChannel(mFmCommonChannels.get(i).getFreq()));
            }
        } else {
            for (int i = 0; i < 4; i++) {
                mTempChannels.add(new TempChannel(mAmCommonChannels.get(i).getFreq()));
            }
        }
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        view = (VolumeLayout) LayoutInflater.from(container.getContext()).inflate(
                R.layout.fm_pager, null);

        view.setOnClickListener(new VolumeLayout.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        container.addView(view);

        mTvFreq = (TextView) view.findViewById(R.id.fm_hz);
        mTvFreq.setText(mTempChannels.get(position).getFreq() + "");

        /**
         * 长按进入微调界面
         *
         */
        mTvFreq.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                XFMApp.getHandler().sendEmptyMessage(101);
                return false;
            }
        });

        return view;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    public void setShowFreq(double freq, int curVpPosition) {
        mFreq = freq;
        this.curVpPosition = curVpPosition;

        //  当非常用频道时，更改当前数据
        mTempChannels.set(curVpPosition, new TempChannel(mFreq));
    }

    //  重置数据
    public void resetShowFreq(boolean isFmMode, List<FmChannel> mFmCommonChannels, List<AmChannel> mAmCommonChannels) {
        this.isFmMode = isFmMode;
        mTempChannels.clear();
        if (isFmMode) {
            for (int i = 0; i < 4; i++) {
                mTempChannels.add(new TempChannel(mFmCommonChannels.get(i).getFreq()));
            }
        } else {
            for (int i = 0; i < 4; i++) {
                mTempChannels.add(new TempChannel(mAmCommonChannels.get(i).getFreq()));
            }
        }
    }
}
