package com.xui.xfm.adapter;

import android.graphics.Color;
import android.logic.XuiManager;
import android.os.RemoteException;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.xui.xfm.R;
import com.xui.xfm.XFMApp;
import com.xui.xfm.bean.AmChannel;
import com.xui.xfm.bean.FmChannel;
import com.xui.xfm.utils.SharedPrefUtils;
import com.xui.xfm.view.FreqView;

import java.util.List;

/**
 * Created by gcf on 2016/8/24.
 */
public class MyListAdapter extends BaseAdapter {

    private static final String TAG = "MyListAdapter";

    private XuiManager mXuiManager;

    private List<FmChannel> mFmChannels;
    private List<AmChannel> mAmChannels;
    private boolean isFmMode;

    private int selectItem;

    public MyListAdapter(boolean isFmMode, List<FmChannel> mFmChannels, List<AmChannel> mAmChannels) {
        this.isFmMode = isFmMode;
        if (isFmMode) {
            this.mFmChannels = mFmChannels;
        } else {
            this.mAmChannels = mAmChannels;
        }

        mXuiManager = XFMApp.getXuiManager();
    }


    @Override
    public int getCount() {
        if (isFmMode) {
            if (mFmChannels != null) {
                return mFmChannels.size();
            }
        } else {
            if (mAmChannels != null) {
                return mAmChannels.size();
            }
        }

        return 0;
    }

    @Override
    public Object getItem(int position) {
        if (isFmMode) {
            if (mFmChannels != null) {
                return mFmChannels.get(position);
            }
        } else {
            if (mAmChannels != null) {
                return mAmChannels.get(position);
            }
        }
        return 0;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder = null;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.fm_list_item, null);
            viewHolder.mTextView = (TextView) convertView.findViewById(R.id.lv_fm_hz);
            viewHolder.mImageView = (FreqView) convertView.findViewById(R.id.list_freq_pic);
            viewHolder.mBgView = convertView.findViewById(R.id.lv_item_bg);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        if (isFmMode) {
            viewHolder.mTextView.setText(mFmChannels.get(position).getFreq() + "");
        } else {
            viewHolder.mTextView.setText(mAmChannels.get(position).getFreq() + "");
        }

        //  选中效果
        if (position == selectItem) {
            viewHolder.mBgView.setVisibility(View.VISIBLE);
            viewHolder.mTextView.setSelected(true);
            viewHolder.mTextView.setShadowLayer(4, 0, 0, R.color.fm_list_selected_color_bg);
            viewHolder.mImageView.setVisibility(View.VISIBLE);
            notifyDataSetChanged();
        } else {
            convertView.setBackgroundColor(Color.TRANSPARENT);
            viewHolder.mImageView.setVisibility(View.GONE);
            viewHolder.mBgView.setVisibility(View.GONE);
            viewHolder.mTextView.setSelected(false);
            viewHolder.mTextView.setShadowLayer(0, 0, 0, R.color.fm_list_selected_color_bg);
        }
        return convertView;
    }

    class ViewHolder {
        public TextView mTextView;
        public FreqView mImageView;
        public View mBgView;
    }

    // 选中的频道
    public void setSelectItem(int mSelectItem) {
        selectItem = mSelectItem;
        Log.d(TAG, " selectItem : " + selectItem);
        notifyDataSetChanged();
        if (isFmMode) {
            SharedPrefUtils.putFmInitNum("initFmNum", selectItem + 1, XFMApp.getContext());
            //  指定频率
            try {

                mXuiManager.setFmFreq(mFmChannels.get(selectItem).getFreq(), selectItem + 1);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        } else {
            SharedPrefUtils.putAmInitNum("initAmNum", selectItem + 1, XFMApp.getContext());
        }
    }

}
